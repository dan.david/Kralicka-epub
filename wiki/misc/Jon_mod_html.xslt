<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Úprava stažené knihy Job v xhtml:
       přesunutí verše Jon1:17 na Jon2:1 a přečíslování veršů v Jon2.
  -->

  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no" />

  <xsl:template match="/">
    <xsl:choose>
        <xsl:when test="not(//p/sup[@id='1:17'])">
           <xsl:message terminate="yes">JIŽ UPRAVENO</xsl:message>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates/>
        </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="sup[@id='1:17']"/>

  <xsl:template match="text()[preceding-sibling::sup[@id='1:17']]"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="sup[substring-before(@id,':')='2']">
    <xsl:variable name='cislo-v' select="number(substring-after(@id,':')) + 1"/>
    <xsl:element name='sup'>
      <xsl:attribute name='id'>
        <xsl:text>2:</xsl:text><xsl:value-of select="$cislo-v"/>
      </xsl:attribute>
      <xsl:value-of select="$cislo-v"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="sup[@id='2:1']">
    <sup id='2:1'><xsl:text>1</xsl:text></sup><xsl:copy-of select="//sup[@id='1:17']/following::text()[1]"/><br/>
    <sup id='2:2'><xsl:text>2</xsl:text></sup>
  </xsl:template>

</xsl:stylesheet>

#!/usr/bin/env perl

# Úprava stažené knihy Jon v JSON formátu:
#   přesunutí verše Jon1:17 na Jon2:1 a přečíslování veršů v Jon2.
# Skript funguje jako filter.

use FindBin;
use lib "$FindBin::Bin/../../perl5";
use BK::Json;

use strict;
use warnings;
use utf8;

# STDIN and STDOUT should be :raw (not :utf8) because of JSON
binmode( STDERR, ':encoding(utf8)' );

my $j = BK::Json->new;
my $d = $j->rd_json_fh( *STDIN );
my $t = $d->{revisions}->[0]->{content};

$t =~ s/(?<=\{\{verš\|kapitola=2\|verš=)(\d+)(?=\}\})/$1+1/ge;
$t =~ s/(?<=\{\{verš\|kapitola=)1\|verš=17(?=\}\})/2|verš=1/ or die "JIŽ UPRAVENO\n";  # exit if mod has been done

$d->{revisions}->[0]->{content} = $t;

print $j->json->encode( $d ), "\n";


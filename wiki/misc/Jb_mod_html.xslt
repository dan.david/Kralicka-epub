<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Úprava stažené knihy Jb v xhtml:
       přesunutí veršů Jb40:1-5 na Jb39:31-35 a přečíslování veršů v Jb40.
  -->

  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no" />

  <xsl:template match="/">
    <xsl:choose>
        <xsl:when test="//p/sup[@id='39:35']">
           <xsl:message terminate="yes">JIŽ UPRAVENO</xsl:message>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates/>
        </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="sup[substring-before(@id,':')='40']">
    <xsl:variable name='cislo-v' select="number(substring-after(@id,':')) - 5"/>
    <xsl:element name='sup'>
      <xsl:attribute name='id'>
        <xsl:text>40:</xsl:text><xsl:value-of select="$cislo-v"/>
      </xsl:attribute>
      <xsl:value-of select="$cislo-v"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="sup[following-sibling::sup[@id='40:6']]"/>
  <xsl:template match="text()[following-sibling::sup[@id='40:6']]"/>

  <xsl:template match="p[sup[@id='39:30']]">
    <xsl:copy>

      <xsl:apply-templates select="@*|node()"/><br/>

      <sup id='39:31'><xsl:text>31</xsl:text></sup><xsl:copy-of select="//sup[@id='40:1']/following::text()[1]"/><br/>
      <sup id='39:32'><xsl:text>32</xsl:text></sup><xsl:copy-of select="//sup[@id='40:2']/following::text()[1]"/><br/>
      <sup id='39:33'><xsl:text>33</xsl:text></sup><xsl:copy-of select="//sup[@id='40:3']/following::text()[1]"/><br/>
      <sup id='39:34'><xsl:text>34</xsl:text></sup><xsl:copy-of select="//sup[@id='40:4']/following::text()[1]"/><br/>
      <sup id='39:35'><xsl:text>35</xsl:text></sup><xsl:copy-of select="//sup[@id='40:5']/following::text()[1]"/>

    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>

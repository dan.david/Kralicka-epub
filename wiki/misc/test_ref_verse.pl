#!/usr/bin/env perl

#
# Kontrola odkazů v souborech pro ePUB, že odkazují na správný text.
# Spuštění:
#   cat Kralička-EPUB/epub/html/*.html | Kralička-EPUB/wiki/misc/test_ref_verse.pl
#

use Encode;
use FindBin;
use lib "$FindBin::Bin/../../perl5";
use utf8;
use open qw( :encoding(utf8) :std );

use BK::RefVerse;

use strict;
use warnings;

my $max_edit_dist = 3;
my $fn = 'bk_referencované_verše.json';
my $r = BK::RefVerse->new( edist => $max_edit_dist )->init( $FindBin::Bin .
             encode( 'UTF-8', "/../text/$fn" ) );

while ( <> ) {

  next unless m{<span +id="([^"]+)">\d+</span>(.*)<(?:br */|/pr)>$};

  my $id = $1;
  my $t = $2;

  $id =~ s/^[vk]//;

  my $s = $r->v_r_match( $id, $r->v_r_mod( $t ) );
  unless ( defined $s ) {
    print "$id - odkaz nenalezen v souboru '$fn'\n";
    next;
  }

  my $sp = ' ' x ( 9 - length( $id ) );
  print "$id $sp", '*' x $max_edit_dist, ">  :(\n" if $s == -1;
  print "$id $sp", '*' x $s, "\n" if $s > 0;

  # if ( $s != 0) {
  if ( $s < 0 ) {
    print " ", $r->v_r_mod( $t ), "\n";
    print " ", $r->get_r_verse( $id ), "\n";
  }
}

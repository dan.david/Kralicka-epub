#!/usr/bin/env perl

# Úprava stažené knihy Jb v JSON formátu:
#   přesunutí veršů Jb40:1-5 na Jb39:31-35 a přečíslování veršů v Jb40.
# Skript funguje jako filter.

use FindBin;
use lib "$FindBin::Bin/../../perl5";
use BK::Json;

use strict;
use warnings;
use utf8;

# STDIN and STDOUT should be :raw (not :utf8) because of JSON
binmode( STDERR, ':encoding(utf8)' );

my $j = BK::Json->new;
my $d = $j->rd_json_fh( *STDIN );
my $t = $d->{revisions}->[0]->{content};

die "JIŽ UPRAVENO\n" if $t =~ /\{\{verš\|kapitola=39\|verš=35\}\}/;

$t =~ s/(?<=\{\{verš\|kapitola=)40\|verš=([1-5])(?=\}\})/'39|verš='.($1+30)/ge;
$t =~ s/(?<=\{\{verš\|kapitola=40\|verš=)(\d+)(?=\}\})/$1-5/ge;

$d->{revisions}->[0]->{content} = $t;

print $j->json->encode( $d ), "\n";


#!/usr/bin/env perl

package Key::RSA;

use Crypt::OpenSSL::RSA;
use Crypt::OpenSSL::Bignum;
use Carp;

use strict;
use warnings;

sub new
{
  my ( $class, $key_txt ) = @_;
  my $key_obj = Crypt::OpenSSL::RSA->new_private_key( $key_txt );
  confess "Did not get a new Crypt::OpenSSL::RSA object" unless $key_obj;
  $key_obj->use_pkcs1_padding;
  return bless { type => 'RSA',
                 key => $key_obj };
}

sub new_from_params
{
  my ( $self, $n_bin, $e_bin ) = @_;

  my $n = Crypt::OpenSSL::Bignum->new_from_bin( $n_bin );
  my $e = Crypt::OpenSSL::Bignum->new_from_bin( $e_bin );
  my $key_obj = Crypt::OpenSSL::RSA->new_key_from_parameters( $n, $e );
  confess "Did not get a new Crypt::OpenSSL::RSA object" unless $key_obj;
  return bless { type => 'RSA',
                 key => $key_obj };
}

sub public_key_params
{
  my $self = shift;

  #  n, e, d, p, q, d mod (p-1), d mod (q-1), and 1/q mod p
  #    where p and q are the prime factors of n, e is the public exponent and d is the private exponent.
  my ( $n, $e ) = ( $self->{ key }->get_key_parameters )[ 0, 1 ]; # bigNum objects
  return $n->to_bin, $e->to_bin;
}

package Signature;

use Digest::SHA qw( sha1 );
use XML::CanonicalizeXML;
use MIME::Base64;
use XML::XPath;
use Carp;
use strict;
use warnings;

use constant XML_TRANS_C14N => 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315';
use constant XML_TRANS_C14N_COMM => XML_TRANS_C14N . '#WithComments';
use constant XMLDSIF => 'http://www.w3.org/2000/09/xmldsig#';
use constant XMLDSIF_SHA1 => XMLDSIF . 'sha1';

sub _c14n
{
  return XML::CanonicalizeXML::canonicalize( @_, '<XPath>(//.|//@*|namespace::*)</XPath>', [], 0, 0 );
}

sub _rdfile
{
  my $fn = shift;
  open( my $fh, '<', $fn ) or confess "$!";
  my $txt;
  {
    local $/;
    $txt = <$fh>;
  }
  close $fh;
  return $txt;
}

sub _rdxml
{
  return _c14n( _rdfile( shift ) );
}

sub sign
{
  my %parms = @_;
  my $key_info;

  {
    my ( $n, $e ) = $parms{ key }->public_key_params;
    $key_info = q{<KeyInfo><KeyValue><RSAKeyValue>
 <Modulus>} . encode_base64( $n, '' ) . q{</Modulus>
 <Exponent>} . encode_base64( $e, '' ) . q{</Exponent>
</RSAKeyValue></KeyValue></KeyInfo>};
  }

  my $manifest = q{<Manifest Id="ManifestBK">
};
  for my $f ( @{ $parms{ manifest } } ) {
    if ( $f =~ /\.xml$/ or $f =~ /\.opf$/ ) {
      my $t = _rdxml( $f );
      $manifest .= qq{<Reference URI="$f">
 <Transforms>
  <Transform Algorithm="} . XML_TRANS_C14N . q{"/>
 </Transforms>
 <DigestMethod Algorithm="} . XMLDSIF_SHA1 . q{"/>
 <DigestValue>} . encode_base64( sha1( $t ), '' ) . q{</DigestValue>
</Reference>
};
    }
    elsif ( $f =~ /\.html$/ or
            $f =~ /\.jpe?g$/ or $f =~ /\.png$/ or $f =~ /\.gif$/ ) {
      $manifest .= qq{<Reference URI="$f">
 <DigestMethod Algorithm="} . XMLDSIF_SHA1 . q{"/>
 <DigestValue>} . encode_base64( sha1( _rdfile( $f ) ), '' ) . q{</DigestValue>
</Reference>
};
    }
    else {
      carp "File $f is not included in the manifest";
    }
  }  # for
  $manifest .= q{</Manifest>};

  my $signed_info =
q{<SignedInfo>
 <CanonicalizationMethod Algorithm="} . XML_TRANS_C14N . q{"/>
 <SignatureMethod Algorithm="} . XMLDSIF_SHA1 . q{"/>
 <Reference URI="#ManifestBK">
  <Transforms>
   <Transform Algorithm="} . XML_TRANS_C14N . q{"/>
  </Transforms>
  <DigestMethod Algorithm="} . XMLDSIF_SHA1 . q{"/>
  <DigestValue>} .
 encode_base64( sha1( _c14n( $manifest ) ), '' )
. q{</DigestValue>
 </Reference>
</SignedInfo>};

  my $signed_info_signature = encode_base64( $parms{ key }->{ key }->sign( _c14n( $signed_info ) ), '' );

  return qq{<signatures xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
<Signature Id="BK-Signature" xmlns="} . XMLDSIF . qq{">
$signed_info
<SignatureValue>$signed_info_signature</SignatureValue>
$key_info
<Object>
$manifest
</Object>
</Signature>
</signatures>};
}  # sign

sub _get_node_as_xml_1
{
  my ( $parser, $xpath ) = @_;
  return XML::XPath::XMLParser::as_string( $parser->findnodes( $xpath )->get_node( 1 ) );
}

sub _node_as_xml
{
  my ( $node ) = @_;
  return XML::XPath::XMLParser::as_string( $node );
}

sub _refs
{
  my $r = shift;

  my $uri = $r->findvalue( './@URI' );
  my $alg = $r->findvalue( './DigestMethod/@Algorithm' );
  my $d = $r->findvalue( './DigestValue' );
  my $t = $r->findnodes( './Transforms/Transform' );
  return ( $uri, $alg, $d, $t );
}

sub _trans
{
  my ( $xml, $method, $trans ) = @_;

  for my $t ( $trans->get_nodelist ) {
    my $alg = $t->findvalue( './@Algorithm' );
    if ( $alg eq XML_TRANS_C14N ) {
      $xml = _c14n( $xml );
    }
    else {
      confess( "Transform algorithm $alg not implemented" );
    }
  }

  if ( $method eq XMLDSIF_SHA1 ) {
    return sha1( $xml );
  }
  else {
    confess( "Digest method $method not implemented" );
  }
}

sub verify
{
  my $parser = XML::XPath->new( xml => _rdfile( $_[0] ) );

  my $signed_node = $parser->findnodes( '//Signature[@Id="BK-Signature"]/SignedInfo' );
  confess ( "BK-signature not found in the Signature" ) if $signed_node->size != 1;
  $signed_node = $signed_node->get_node( 1 );

  my $key_info_set = $parser->findnodes( '//Signature/KeyInfo/KeyValue/RSAKeyValue' );
  if ( $key_info_set and $key_info_set->size == 1 ) {
    my $m_b64 = $key_info_set->get_node( 1 )->findvalue( './Modulus' );
    my $e_b64 = $key_info_set->get_node( 1 )->findvalue( './Exponent' );
    my $key = Key::RSA->new_from_params( decode_base64( $m_b64 ), decode_base64( $e_b64 ) );

    my $signature = $parser->findvalue( '//Signature/SignatureValue' );
    my $signed_xml_c14n = _c14n( _node_as_xml( $signed_node ) );
    my $v = $key->{key}->verify( $signed_xml_c14n, decode_base64( $signature ) );
    unless ( $v ) { return -1; }
  }
  else {
    confess 'KeyInfo not found in the Signature';
  }

  my ( $signed_uri, $signed_method, $signed_value, $signed_transforms ) = _refs( $signed_node->find( './Reference' )->get_node( 1 ) );
  $signed_uri =~ s/#//;
  my $manifest_node = $parser->findnodes( "//Signature[\@Id='BK-Signature']/Object/*[\@Id='$signed_uri']" )->get_node(1);
  my $manifest_xml = _node_as_xml( $manifest_node );
  my $manifest_sign = _trans( $manifest_xml, $signed_method, $signed_transforms );
  # confess "Manifest Digest value does not match" if decode_base64( $signed_value ) ne $manifest_sign;
  return -2 if decode_base64( $signed_value ) ne $manifest_sign;

  for my $t ( $manifest_node->findnodes( './Reference' )->get_nodelist ) {
    my ( $uri, $method, $signed_value, $transforms ) = _refs( $t );
    my $xml = _rdfile( $uri );
    my $digest = _trans( $xml, $method, $transforms );
    # confess "$uri Digest value does not match" if decode_base64( $signed_value ) ne $digest;
    return -3 if decode_base64( $signed_value ) ne $digest;
  }

  return 1;

}  # verify

package eBook;

use File::Temp;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Carp;

use strict;
use warnings;

use constant EPUB_Signature_fn => 'META-INF/signatures.xml';

sub open_ebook
{
  my ( $class, $file ) = @_;
  my $self = {};
  bless $self, $class;

  $self->{ tmp_dir } = File::Temp->newdir;
  $self->{ tmp_dir_name } = $self->{ tmp_dir }->dirname;

  my $zip = Archive::Zip->new;
  my $zip_status = $zip->read( $file );
  confess "Could not read ZIP file" if $zip_status != AZ_OK;

  $self->{ members_count } = $zip->numberOfMembers;
  @{ $self->{ members } } = $zip->memberNames;
  $zip_status = $zip->extractTree( { zipName => $self->{ tmp_dir_name } } );
  confess "Could not extract ZIP archive" if $zip_status != AZ_OK;
  return $self;
}

sub add_members
{
  my $self = shift;
  push @{ $self->{ members } }, @_;
  $self->{ members_count } += @_;
}

sub write_ebook
{
  my ( $self, $file ) = @_;
  my $zip = Archive::Zip->new;

  for my $f ( @{ $self->{ members } } ) {
    my $zip_level = $f =~ /mimetype/ ? COMPRESSION_STORED : COMPRESSION_DEFLATED ;

    my $f_tmp = $self->{ tmp_dir_name } . '/' . $f;
    next if -d $f_tmp;
    $zip->addFileOrDirectory( { name => $f_tmp, zipName => $f, compressionLevel => $zip_level } );
  }
  my $zip_status = $zip->writeToFileNamed( $file );
  confess "Error writing to ZIP: $zip_status" if $zip_status != AZ_OK;
}

sub add_signature
{
  my $self = shift;
  open ( my $fh, '>', $self->{ tmp_dir_name } . '/' . EPUB_Signature_fn ) or confess "Could not open file: $!";
  print $fh qq{<?xml version="1.0"?>\n};
  print $fh $_[0], "\n";
  close $fh;
  $self->add_members( EPUB_Signature_fn );
}

sub ebook_dir
{
  return $_[0]->{tmp_dir_name };
}

sub DESTROY
{
  chdir '/tmp'; # to allow remove tmp dir
}


package main;

use Getopt::Std;
use Cwd;

use strict;
use warnings;

sub klic
{
  my $fn = shift;
  die "You need to provide the private key!" unless defined $fn;

  my $k_obj;
  if ( open my $fh, '<', $fn ) {
    my $txt;
    {
      local $/;
      $txt = <$fh>;
    }
    close $fh;

    if ( $txt =~ m/BEGIN ([DR]SA)? PRIVATE KEY/ )
    {
      if ( not defined $1 or $1 eq 'RSA' ) {
        $k_obj = Key::RSA->new( $txt );
      }
      else { 
        die 'DSA is not implemented';
        # $k_obj = Key::DSA->new( $txt );
      }
      return $k_obj;
    }
    else {
      die "Could not detect type of key in $fn.";
    }
  }
  else {
    die "Could not load key in $fn: $!";
  }
}

# --------------

our %opts;
getopts( 'sk:e:vh', \%opts) or die "Parameters are not correct";
if ( $opts{h} ) {
  print << "EOT";
Usage: $0 -s -k key -e book -s -v -h
  -h this help
  -s insert signatures.xml file into e-book
  -v verify signatures.xml
  -k private key (RSA)
  -e e-book
EOT
exit;
}

die 'Epub book is not provided' unless defined $opts{e};
die 'Sign needs a Key' if defined $opts{s} and not defined $opts{k};
die '???' unless defined $opts{v} xor defined $opts{s};

my $wrk_dir = getcwd;
if ( $opts{s} ) {
  my $klic = klic( $opts{k} );
  my $ekniha = eBook->open_ebook( $opts{e} );
  my $ekniha_adr = $ekniha->ebook_dir;
  # cwd is ebook

  my @manifest;
  for my $f ( @{ $ekniha->{members} } ) {
    next unless -f "$ekniha_adr/$f";
    push @manifest, $f if
      $f =~ m|OPS/text/.*\.xml|
          or
      $f =~ /\.opf$/
          or
      $f =~ m|OPS/text/.*\.html|
          or
      $f =~ m|OPS/images/|;
  }

  chdir $ekniha_adr or die "Could not chdir: $!";
  $ekniha->add_signature( Signature::sign ( key => $klic, manifest => \@manifest ) );
  chdir $wrk_dir;

  $opts{e} =~ s/\.epub$/.signed.epub/ or $opts{e} .= "-signed";
  $ekniha->write_ebook( $opts{e} );
}
elsif ( $opts{v} ) {
  my $ekniha = eBook->open_ebook( $opts{e} );
  my $ekniha_adr = $ekniha->ebook_dir;
  chdir $ekniha_adr or die "Could not chdir: $!";
  my $v = Signature::verify( eBook::EPUB_Signature_fn );
  chdir $wrk_dir;
  print "Verify: ", $v == 1 ? 'OK' : "failed ($v)", "\n";
}
else {
  die '???';
}

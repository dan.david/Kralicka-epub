#!/usr/bin/env perl

use Text::Diff;
use FindBin;
use lib "$FindBin::Bin/../../perl5";
use utf8;
use open qw { :encoding(utf8) :std };

use BK::Wiki::Json;

use strict;
use warnings;

sub bk_text;
sub wiki_file;

unless ( @ARGV == 2 ) {
  print <<EOT;
Spuštění:
  $0 souboru_1 soubor_2

 Skript sestaví text knih v souborech (verš na jednom řídku)
 a pomocí diff-u oba texty porovná.
EOT

  exit;
}

my ( $fn_1, $fn_2 ) = @ARGV;
print "\nDiff $fn_1 $fn_2\n";
die "soubor '$fn_1' nenalezen\n" unless -f $fn_1;
die "soubor '$fn_2' nenalezen\n" unless -f $fn_2;

if ( $fn_1 =~ /\.json/ or $fn_2 =~ /\.json/ ) { require BK::Wiki::Json; }
if ( $fn_1 =~ /\.html/ or $fn_2 =~ /\.html/ ) { require BK::Wiki::Html; }

my $bk_1 = wiki_file $fn_1;
my $bk_2 = wiki_file $fn_2;

die $fn_1, ": text nenalezen\n" unless defined $bk_1->bk;
die $fn_2, ": text nenalezen\n" unless defined $bk_2->bk;

print " změna: ", $bk_1->bk_meta->{author} // '' , ' ', $bk_1->bk_meta->{timestamp} // '' , "\n";
print " změna: ", $bk_2->bk_meta->{author} // '' , ' ', $bk_2->bk_meta->{timestamp} // '' , "\n";

my @txt_1; bk_text( $bk_1, \@txt_1 );
my @txt_2; bk_text( $bk_2, \@txt_2 );

my $v;
diff \@txt_1, \@txt_2, { STYLE => Text::Diff::Unified->new, CONTEXT => 0, OUTPUT => \$v };
print defined $v ? $v : 'OK', "\n";

sub bk_text
{
  my ( $b, $r ) = @_;

  for my $ch ( $b->bk->all_items ) {
    for my $v ( $ch->all_items ) {
      my $v_id = $v->v_id;
      push @{ $r },  $ch->name . "_" . $v_id . ": " . $b->text_utf8( $v->text ) . "\n";
    }
  }
}

sub wiki_file
{
  my $fn = shift;
  open( my $fh, '<:', $fn ) or die "chyba při otevírání souboru $fn: $!\n";
  return $fn =~ /\.json/ ?
      BK::Wiki::Json->new->parse( $fh ) :
      BK::Wiki::Html->new->parse( $fh ) ;
}

__END__

Příklad výstupu:

Diff ../json/Gn.json.bak ../json/Gn.json
 změna: WikiUser 2015-08-30T08:24:56Z
 změna: WikiUser 2015-08-30T08:24:56Z
OK

Diff ~/Kralicka_git/wiki/json/Gn.json.bak ~/Kralicka_git/wiki/json/Gn.json
 změna: WikiUser 2015-08-30T08:24:56Z
 změna: WikiUser 2015-08-30T08:24:56Z
@@ -5 +5 @@
-1_5: A nazval Bůh světlo dnem, a tmu nazval nocí. I byl večer a bylo jitro, den PRVNÍ.
+1_5: A nazval Bůh světlo dnem, a tmu nazval nocí. I byl večer a bylo jitro, den první.


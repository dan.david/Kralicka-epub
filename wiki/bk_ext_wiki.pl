#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::Bin/../perl5";
use utf8;


package BK::Info;
use Moose;

use BK::Books;
use BK::Regexp;

has books_data => (
  is => 'ro',
  isa => 'BK::Books',
  default => sub { BK::Books->new; },
);

has misc_text => (
  is => 'ro',
  isa => 'BK::MiscText',
  required => 1,
);

has book_id => (
  is => 'rw',
  isa => 'Str',
  required => 1,
  lazy => 1,
  default => undef,
);

has rex => (
  is => 'ro',
  isa => 'BK::Regexp',
  default => sub { BK::Regexp->new; },
);

# count of verses in the book
has book_verse_cnt => (
  is => 'rw',
  isa => 'Int',
  default => 0,
);

has txt_refs => (
  is => 'rw',
  isa => 'HashRef[Str]',
  traits => ['Hash'],
  handles => {
    exists_ref => 'exists',
    get_ref_type => 'get',
    add_ref => 'set',
  },
  default => sub { {} },
);

sub BUILD
{
  my $self = shift;
  die "$0: v argumentech zadáno neznámé ID knihy: " . $self->book_id
    unless $self->books_data->b_exists( $self->book_id );
}

__PACKAGE__->meta->make_immutable;

no Moose;


package BK::XML::Writer;

use parent 'XML::Writer';

use strict;
use warnings;

sub pod_text
{
  # [text] and {text} to I<text> and Y<text>, resp.
  no warnings qw{ uninitialized };
  $_[1] =~ s/\[([^\]]+)\]([\.:,])?/I<$1$2>/g;
  # $_[1] =~ s/{([^}]+)}([\.:,])?/Y<$1$2>/g;
  use warnings qw{ uninitialized };

  # text between {} will be (probably) removed
  $_[1] =~ s/{([^}]+)}(\s)/Y<$1$2>/g;
  $_[1] =~ s/(\s)\{([^}]+)\}/Y<$1$2>/g;
  return not $_[1] =~ /[{}]/;
}

sub tag_text
{
  my ( $self, $bk_i, $t ) = @_;
  my $t_pos = 0;  # index of processed text

  my $rex = $bk_i->rex->pod_tag_cap;
  while ( $t =~ /$rex/g ) {
    my $pre = substr( $t, $t_pos, $-[0] - $t_pos );
    $t_pos = $+[0];
    my $el = $1;  # pod element
    my $el_id = substr( $el, 0, 1 );
    my $el_txt = substr( $t, $-[1] + 2 , $+[1] - $-[1] - 3 ); # remove 'X<' '>'

    $self->characters( $pre ) if $pre;
    my $tagText;
    if ( $el_id eq 'I' ) {
      $self->startTag( 'alt_text' );
      $tagText = $self->tag_text( $bk_i, $el_txt );
      $self->endTag( 'alt_text' );
    }
    elsif ( $el_id eq 'B' ) {
      $self->startTag( 'iniciála' );
      $tagText = $self->tag_text( $bk_i, $el_txt );
      $self->endTag( 'iniciála' );
    }
    elsif ( $el_id eq 'L' ) {
        # valid ref
      my ( $text, $link ) = split( /\|/, $el_txt );
      my $ref_type = $bk_i->get_ref_type( $link );
      if ( $ref_type eq 'v_ref' ) {
        $self->startTag( 'v_ref', 'v_id' => $link );
        $tagText = $self->tag_text( $bk_i, $text ) if defined $text;
        $self->endTag( 'v_ref' );
      }
      elsif ( $ref_type eq 'k_ref' ) {
        my $b_id = ( split( '_', $link ) )[0];
        if ( $bk_i->book_id eq $b_id )
          { $self->startTag( 'k_ref', 'k_id' => $link ); }
          else { $self->startTag( 'k_ref', 'b_id' => $b_id, 'k_id' => $link ); }
        $tagText = $self->tag_text( $bk_i, $text ) if defined $text;
        $self->endTag( 'k_ref' );
      }
      else {
        warn $bk_i->book_id,
             ": neznámý typ interní 'POD' značky 'L': '$ref_type'";
      }
    }
    elsif ( $el_id eq 'Y' ) {
      $self->startTag( 'alt_text_2' );
      $self->tag_text( $bk_i, $el_txt );
      $self->endTag( 'alt_text_2' );
    }
    elsif ( $el_id eq 'U' ) {
      $self->startTag( 'iniciála_vnitřní' );
      $self->tag_text( $bk_i, $el_txt );
      $self->endTag( 'iniciála_vnitřní' );
    }
    else { warn $bk_i->book_id, ": neznámá interní 'POD' značka: '$el_id'"; }

  } # while

  $self->characters( substr( $t, $t_pos ) ) if $t_pos < length( $t );
} # tag_text


package Opts;
use Moose;

use Getopt::Std;

has [ qw/ fn_in fn_out / ]  => (
  is => 'rw',
  isa => 'Str',
  required => 1,
);

has book_id => (
  is => 'rw',
  isa => 'Str',
  required => 1,
);

has html_src => (
  is => 'ro',
  isa => 'Bool',
  default => 0,
);

sub usage
{
  print << "EOT";
Spuštění: $0 -i {JSON/HTML}_vstupní_soubor -o XML_výstupní_soubor -b ID-knihy [ -j -h ]
  -H zpracování HTML souborů, imlicitně se používá JSON formát
  -h tento stručný popis
Více v: perldoc $0
EOT
}

sub BUILDARGS
{
  my %opts;
  getopts( 'o:i:b:Hh', \%opts ) or die "$0: chybné parametry skriptu";
  if ( defined $opts{h} ) { usage; exit; }

  return { fn_in => $opts{i}, fn_out => $opts{o}, book_id => $opts{b},
           html_src => defined $opts{H},
         };
}

__PACKAGE__->meta->make_immutable;

no Moose;


package main;

use File::Copy;
use BK::MiscText;
use BK::Initials;
use BK::Lib;

use strict;
use warnings;
use utf8;
use open qw( :encoding(utf8) :std );

sub bk_check_L
{
  # Args: book-info
  my $bk_i = shift;

  my $ret = 0; # not modified
  my $t_pos = 0;

  my $pod_rex = $bk_i->rex->pod_tag_cap;
  while ( $_[0] =~ /$pod_rex/g ) {
     my $t_pre = substr( $_[0], $t_pos, $-[0] - $t_pos );
     $t_pos = $+[0];

     my $link_pos = $-[1];  # pos of 'L<'
     my $link_text = $1;

     warn $bk_i->book_id, ": zřejmě chyně zapsaný odkaz: '$t_pre'"
       if $t_pre =~ /[[:upper:]]<|>/;
     next unless substr( $link_text, 0, 1 ) eq 'L';

     $link_text = substr( $link_text, 2, -1 );  # remove 'L<' and '>'
     my ( $ref_txt, $ref_id ) = split( '\|', $link_text, 2 );

     my ( $ref_b_id, $ref_ch_id, $ref_v_id );
     my $rm_link = 0;
     {
       unless ( defined $ref_id and defined $ref_txt ) {
         warn $bk_i->book_id,
              ": odkaz nemá 2 části (odkaz se ignoruje): '$link_text'";
         $rm_link = 1;
         next;
       }

       unless ( $ref_id =~ /^(\d?[[:alpha:]]+)_(\d+)(?:_(\d+))?$/ ) {
         warn $bk_i->book_id,
              ": chybný formát odkazu na verš/kapitolu (odkaz se ignoruje): '$ref_id'";
         $rm_link = 1;
         next;
       }
       ( $ref_b_id, $ref_ch_id, $ref_v_id ) = ( $1, $2, $3 );

       unless ( $bk_i->books_data->b_exists( $ref_b_id ) and
                $bk_i->books_data->get_ch_count( $ref_b_id ) >= $ref_ch_id ) {
         warn $bk_i->book_id,
              ": odkaz na neznámou kapitolu (odkaz se ignoruje): '$ref_id'"; 
         $rm_link = 1;
         next;
       }
     }
     if ( $rm_link ) {
       substr( $_[0], $link_pos, length( $link_text ) + 3 ) = '';
       pos( $_[0] ) = $link_pos;
       $ret = 2;
       next;
     }

     my $ref_type;
     if ( defined $ref_v_id ) {
       # a verse link
       $ref_type = 'v_ref';
       warn $bk_i->book_id,
            ": info: nesouhlasí text reference s číslem veršem: '$ref_txt'"
         unless $ref_txt =~ /\s*$ref_v_id\D/;
     }
     else {
       # a chapter link
       $ref_type = 'k_ref';
     }

     warn $bk_i->book_id, ": info: link do jiné knihy"
       unless ( $ref_b_id eq $bk_i->book_id );

     $bk_i->add_ref( $ref_id, $ref_type );
  } # while( L<> )

  return $ret;

} # bk_check_L


sub bk_check_link
{
  # Args: book-info, chapter_ID
  my ( $bk_i, $ch_id ) = @_;
  my $t;

  # 1st - Synopse
  $t = $bk_i->misc_text->get_chapter_syn( $bk_i->book_id, $ch_id );
  if ( defined $t and $t ne '' ) {
    bk_check_L( $bk_i, $t ) and
      $bk_i->misc_text->set_chapter_syn( $bk_i->book_id, $ch_id, $t );
  }
  else {
    warn $bk_i->book_id, "_$ch_id: kapitola bez sumáře";
  }

  # 2nd - Notes
  $t = $bk_i->misc_text->get_chapter_pre( $bk_i->book_id, $ch_id );
  if ( defined $t and bk_check_L( $bk_i, $t ) ) {
    $bk_i->misc_text->set_chapter_pre( $bk_i->book_id, $ch_id, $t );
  }
} # bk_check_link


sub bk_check
{
  # Args: book-data, book-info
  my ( $bk_o, $bk_i ) = @_;

  my $ch_cnt = $bk_o->item_count;  # number of chapters in the book

  warn $bk_i->book_id, ": neočekávaný počet kapitol: $ch_cnt"
    if $ch_cnt != ( $bk_i->books_data->get_ch_count( $bk_i->book_id ) // 0 );

  my $b_v_cnt = 0;
  my $ch_seq = 0;
  for my $ch ( $bk_o->all_items ) {
    my $ch_id = $ch->name;  # chapter's name is an integer
    if ( ++$ch_seq != $ch_id ) {
      warn $bk_i->book_id,
           ": nekonzistentní číslo kapitoly, kapitola $ch_id, očekávané $ch_seq";
    }

    my $v_seq = 0;
    my $pod_rex = $bk_i->rex->pod_tag_cap;
    for my $v ( $ch->all_items ) {
      my $v_id = $v->v_id;
      warn $bk_i->book_id,
           "_$ch_id: nekonzistentní číslo verše, verš $v_id, očekávané $v_seq"
        if ++$v_seq != $v_id;
      warn $bk_i->book_id, "_${ch_id}_$v_id: verš bez textu" if $v->text eq '';
      warn $bk_i->book_id, "_${ch_id}_$v_id: nepárová [" if $v->text =~ /\[[^\[\]]+(?:\[|$)/;
      warn $bk_i->book_id, "_${ch_id}_$v_id: nepárová ]" if $v->text =~ /(?:^|\])+[^\[\]]+\]/;
      warn $bk_i->book_id, "_${ch_id}_$v_id: konflikt s POD značkou ve vstupním textu: '$1'"
        if $v->text =~ /$pod_rex/;
    }
    $b_v_cnt += $ch->item_count;
    warn $bk_i->book_id, "_${ch_id}: kapitola bez veršů" if $ch->item_count == 0;

    # check POD L-marks,...
    bk_check_link( $bk_i, $ch_id );

  }  # for (chapters)

  $bk_i->book_verse_cnt( $b_v_cnt );
} # bk_check


# Args: -i {JSON/HTML}_input_file_name  -o XML_output_file_name -b book_Id
# e.g.: -i ./json/Gn.json -o ./xml/Gn.xml -b Gn
my $opts = Opts->new;

unless ( $opts->html_src ) { require BK::Wiki::Json; }
  else { require BK::Wiki::Html; }

my $bk_info = BK::Info->new(
  misc_text => BK::MiscText->new->init(
    qw{ text/bk_synopse_kapitol.json
        text/bk_noticky_kapitol.json
        text/bk_dodatek_knih.json
        text/bk_jména_knih.json
        text/bk_typy_jmen_knih.json } ),
  book_id => $opts->book_id,
 );
my $bk_ini = BK::Initials->new;

open( my $fh_i, '<:', $opts->fn_in )
  or die $opts->fn_in, ": soubor nelze otevřít: $!";
my $bk_data = ( not $opts->html_src ) ?
  BK::Wiki::Json->new->parse( $fh_i ) :
  BK::Wiki::Html->new->parse( $fh_i );
die $opts->fn_in, ": text nenalezen" unless defined $bk_data->bk;

# few simple checks
bk_check $bk_data->bk, $bk_info;

open( my $fh_o, '>', $opts->fn_out . '.tmp' )
  or die $opts->fn_out, ": open failed: $!";
my $w = BK::XML::Writer->new( OUTPUT => $fh_o, ENCODING => 'utf-8' );
$w->xmlDecl();
$w->startTag( "kniha",
              id => $bk_info->book_id,
              kapitol => $bk_data->bk->item_count,
              veršů => $bk_info->book_verse_cnt );

my $b_nm = $bk_info->misc_text->get_book_name( $bk_info->book_id );
if ( defined $b_nm ) {
  $w->startTag( "názvy" );
  $w->dataElement( 'dlouhý', $b_nm->[1] );
  $w->dataElement( 'krátký', $b_nm->[4] );
  my $n = @{$b_nm->[2]};
  $w->startTag( 'rozložený', počet => $n, typ => $b_nm->[0] );
  for my $i ( 1 .. $n ) {
    $w->dataElement( 'řádek', uc( $b_nm->[2]->[$i -1] ), n => $i );
  }
  $w->endTag( 'rozložený' );
  $w->endTag( "názvy" );

  warn $bk_info->book_id, ": chybný počet částí celého jména knihy"
    if $n != $bk_info->misc_text->get_book_title_len( $b_nm->[0] );
}
else { warn $bk_info->book_id, " je bez jména"; }

for my $ch ( $bk_data->bk->all_items ) {
  my $ch_id = $ch->name;
  $w->startTag( 'kapitola', id => $ch_id, veršů => $ch->item_count );

  if ( my $ch_pre_syn = $bk_info->misc_text->get_chapter_pre( $bk_info->book_id, $ch_id ) ) {
    $w->startTag( 'noticka' );
    BK::Lib::vlna( $ch_pre_syn );
    $w->tag_text( $bk_info, $ch_pre_syn );
    $w->endTag( 'noticka' );
  }

  if ( my $ch_syn = $bk_info->misc_text->get_chapter_syn( $bk_info->book_id, $ch_id ) ) {
    $w->startTag( "synopse" );
    BK::Lib::vlna( $ch_syn );
    $w->tag_text( $bk_info, $ch_syn );
    $w->endTag( "synopse" );
  }

  my $initial_on_verse = $bk_ini->get_verse_no( $bk_info->book_id, $ch_id ); # 0 - verse no
  for my $v ( $ch->all_items ) {
    my $t = $bk_data->text_utf8( $v->text );
    BK::Lib::vlna( $t );

    my $id = $bk_info->book_id . "_${ch_id}_" . $v->v_id;
    $w->startTag( 'verš',
                  celé_id => $id,
                  id => $v->v_id,
                  ref => ( $bk_info->exists_ref( $id ) ? 'true' : 'false' ) );
    $bk_ini->tag_ini( $bk_info->book_id, $ch_id, \$t )
      if $v->v_id == $initial_on_verse;
    $w->pod_text( $t ) or warn "$id: nepodařilo se vše přeformátovat";
    $w->tag_text( $bk_info, $t );
    $w->endTag( 'verš' );
  }
  $w->endTag( 'kapitola' );
} # for (chapters)

if ( my $b_post = $bk_info->misc_text->get_book_post( $bk_info->book_id ) ) {
  $w->startTag( "dovětek" );
  BK::Lib::vlna( $b_post );
  $w->pod_text( $b_post )
    or warn "$bk_info->book_id: dovětek: nepodařilo se vše přeformátovat";
  $w->tag_text( $bk_info, $b_post );
  $w->endTag( "dovětek" );
}

$w->endTag( "kniha" );
$w->end;
$fh_o->close;

move $opts->fn_out . '.tmp',
     $opts->fn_out or die "Chyba přejmenování souboru: $!";

__END__

=pod

=encoding utf-8

=head1 NÁZEV

bk_ext_wiki.pl - převedení souborů z Wikisource.org do XML.

=head1 VERZE

Verze 0.1

=head1 SYNOPSE

  ./bk_ext_wiki.pl -i Wiki_stránka.json -o výstup.xml -b ID-knihy [ -H -h ]

=head1 PŘEPÍNACE

=over 4

=item -i Wiki_stránka.json

Jméno vstupního souboru, může být v JSON nebo HTML formátu z Wikisource.org,
viz C<-H>. Implicitně se očekává JSON formát.

=item -o výstup.xml

Jméno výstupního souboru.

=item -b

Id knihy, např. C<Gn> pro knihu Genesis.

=item -H

Vstupní soubor je v HTML formátu.

=item -h

Zobrazení stručné nápovědy.

=back

=head1 POPIS

Skript převede JSON soubor obsahující jednu knihu Bible do XML formátu,
který se následně použije pro transformaci do podoby pro ePUB.

Vstupní soubor se zadává volbou C<-i> a výstupní soubor volbou C<-o>.
Volba C<-b> udává ID-knihy Bible.
Např.:

  ./bk_ext_wiki.pl -i Gn.json -o Gn.xml -b Gn

Pokud je vhodnější použít HTML formát stránek z Wikisource.org,
je třeba skript spustit s přepínačem C<-H>.

Skript provádí několik kontrol:

=over 4

=item *

na počet kapitol v knize

=item *

na konzistentní číslování kapitol a veršů apod.

=back

=head1 AUTOR

E<lt>empekr@ghostmail.comE<gt>

=head1 COPYRIGHT A LICENCE

Copyright (c) 2015 empekr@ghostmail.com

Tento skript je svobodný software; můžete ho redistribuovat a/nebo upravovat
za stejných podmínek jako Perl 5.

This is free software; you can redistribute it and/or modify it under
the same terms as Perl 5 itself.

=cut


#!/usr/bin/env perl

use Text::ParseWords;
use Getopt::Std;
use File::Copy;

use FindBin;
use lib "$FindBin::Bin/../perl5";
use BK::Wiki::RqGap;

use strict;
use warnings;
use utf8;
use open qw( :encoding(utf8) :std );

my %opts;
getopts('hS', \%opts) or die "$0: chybné parametry skriptu";
if ( $opts{h} ) {
  print << "EOT";
Spuštění: $0 -h
  -h tento stručný popis
  -S bez prodlevy mezi požadavky na Wikisource.org
Více v: perldoc $0
EOT
  exit;
}

my $req_delay = BK::Wiki::RqGap->new( \%opts );
my $url = 'https://cs.wikisource.org/wiki/Bible_kralick%C3%A1/';

# cd into a destination directory
chdir "$ENV{PWD}/html" or die "chyba 'cd' do '$ENV{PWD}/html' adresáře: $!";

while ( my $r = <> ) {
  chomp $r;
  next unless $r;

  # 0 - book id , 1 - book name
  my ( $b_id, $p_id, $b_nm ) = parse_line( ',', 0, $r );

  $b_nm =~ s/ /_/g;
  my $b_fn = "$b_id.html";
  my @time_cond = -e $b_fn ? ( '--time-cond', $b_fn ) : () ;

  print "$b_id/$b_nm -> $b_fn\n";
  unlink "$b_fn.tmp" if -e "$b_fn.tmp";
  system ( '/usr/bin/curl',
    '--location', '--remote-time', # '--silent', '--show-error',
    '--output', "$b_fn.tmp",
    @time_cond,
    $url . $b_nm
   ) == 0 or die "'curl' proces skončil s chybou: $! $?";

  if ( -e "$b_fn.tmp" ) {
    if ( @time_cond ) {
      move $b_fn, "$b_fn.bak"
        or die "Chyba přejmenování 'bak' souboru: $!";
    }
    move "$b_fn.tmp", $b_fn or die "Chyba přejmenování souboru: $!";
  }

  $req_delay->delay_content unless STDIN->eof;
}

__END__

=pod

=encoding utf-8

=head1 NÁZEV

bk_get_wiki_html.pl - stažení HTML stránky z Wikisource.org.

=head1 VERZE

Verze 0.1

=head1 SYNOPSE

  cat Wiki_knihy_{Starý,Nový}_zákon.txt | ./bk_get_wiki_html.pl [ -S -h ]

=head1 PŘEPÍNACE

=over 4

=item -S

Nevkládat prodlevu mezi odesílané dotazy na Wikisource.org.

=item -h

Zobrazení stručné nápovědy.

=back

=head1 POPIS

Skript pomocí curl utility stahne HTML stránky
z L<https://cs.wikisource.org/wiki/Bible_kralická/>.

Skript čte na standardním vstupu seznam stránek (tj. knih Bible) ke stažení,
každý řádek obsahuje údaje o jedné knize v tomto CSV formátu:

  ID-knihy,Wiki-ID-knihy,jméno-knihy-v-URL

např.:

  Pis,4431,"Píseň_Šalamounova"

Obsahuje-li vstupní řádek více atributů než tři, jsou ignorovány.
Stažené soubory se ukládají do F<./html> adresáře,
pojmenované jsou podle vzoru: F<ID-knihy.html>.
Soubor s předchozí verzí, pokud existoval, se přejmenuje,
přidá se mu přípona C<.bak>.

Skript resp. C<curl> by měl stahovat pouze novější soubory než jsou lokální.

Skript vkládá prodlevu několik vteřin mezi odeslané požadavky
na Wikisource.org, pokud není uveden přepínač C<-S>.

=head1 AUTOR

E<lt>empekr@ghostmail.comE<gt>

=head1 COPYRIGHT A LICENCE

Copyright (c) 2015 empekr@ghostmail.com

Tento skript je svobodný software; můžete ho redistribuovat a/nebo upravovat
za stejných podmínek jako Perl 5.

This is free software; you can redistribute it and/or modify it under
the same terms as Perl 5 itself.

=cut

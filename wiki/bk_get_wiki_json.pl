#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::Bin/../perl5";

package Query;

use Moose;
use Carp;
use utf8;

has json => (
  is => 'ro',
  isa => 'BK::Json',
);

has rev_prop => (
  is => 'rw',
  isa => 'Str',
);

has bulk_limit => (
  is => 'rw',
  isa => 'Int',
  default => 5,
);

has wiki_pages => (
  is => 'rw',
  isa => 'ArrayRef[Str]',
);

has query_page_id => (
  is => 'rw',
  isa => 'Bool',
);

sub page_count
{
  my $self = shift;
  return scalar @{ $self->wiki_pages };
}

sub req_content
{
  my $self = shift;
  return 'pageids=' . join( '|', splice( @{ $self->wiki_pages }, 0, $self->bulk_limit ) )
    if $self->query_page_id;
  return 'titles=' . join( '|', map { 'Bible_kralick%C3%A1/' . $_ } splice( @{ $self->wiki_pages }, 0, $self->bulk_limit  ) );
}

sub query_wiki
{
  my $self = shift;
  return unless @{ $self->wiki_pages };

  open ( my $p, '-|:raw',
         '/usr/bin/curl', '--location',
         'https://cs.wikisource.org/w/api.php?action=query&prop=revisions&rvprop=' .
            $self->rev_prop .
            '&' . $self->req_content .
            '&format=json&formatversion=2' )
     or croak "Chyba při spuštění curl procesu: $!";

  my $j = $self->json->rd_json_fh( $p );
  close( $p ) or croak "'curl' proces skončil s chybou: $! $?";

  carp "'batchcomplete' v opovědi z Wikisource by měl být 'true', dotaz: rvprop='",
       $self->rev_prop, "' obsah='", $self->req_content, "'"
    unless $j->{batchcomplete};

  # TODO ??? Dumper
  croak "Neočekávaná JSON struktura"
    unless exists $j->{query} and exists $j->{query}->{pages};

  return $j;
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;


package main;

use Text::ParseWords;
use Getopt::Std;
use File::Copy;
use HTTP::Date;

use BK::Json;
use BK::Wiki::RqGap;

use open qw( :encoding(utf8) :std );
use strict;
use warnings;
use utf8;

sub rlst;
sub chkp;
sub gwtm;
sub gwtxt;

use constant B_STATUS_IDX     => 0;
use constant B_BOOK_ID_IDX    => 1;
use constant B_BOOK_URL_IDX   => 2;
use constant B_BOOK_TITLE_IDX => 3;

use constant S_UNDEF  => 0;
use constant S_EXISTS => 1;
use constant S_GET    => 2;
use constant S_DONE   => 3;

my %opts;
getopts('hSBft', \%opts) or die "$0: chybné parametry skriptu";
if ( $opts{h} ) {
  print << "EOT";
Spuštění: $0 -h -S -B
  -h tento stručný popis
  -S bez prodlevy mezi odesíláním dotazů na Wikisource.org
  -B bez zálohy předchozího lokálního souboru
  -f nepodmíněné stahování knih
  -t v dotazu použít jméno Wiki stránky místo Id
Více v: perldoc $0
EOT
  exit;
}

# cd into a destination directory
chdir "$ENV{PWD}/json" or die "chyba 'cd' do '$ENV{PWD}/json' adresáře: $!";

# Read list of Books on STDIN
my %bk_list;
rlst *STDIN => \%bk_list;

# Json, Wiki Query and ...  objects
my $query = Query->new( json => BK::Json->new, query_page_id => not defined $opts{t} );
my $req_delay = BK::Wiki::RqGap->new( \%opts );

my @bk_lst_tmp;

# Get Wiki timestamps and compare with local JSON files
unless ( $opts{f} ) {
  # list of books to check
  @bk_lst_tmp = $query->query_page_id ?
          keys %bk_list :
          map { $bk_list{$_}->[B_BOOK_URL_IDX] } keys %bk_list;
  $query->wiki_pages( \@bk_lst_tmp );

  $query->rev_prop( 'timestamp' );  # requested property
  gwtm \%bk_list, $query, $req_delay;
}

# Get Wiki text
$query->rev_prop( 'content|timestamp|user' );

@bk_lst_tmp = grep { $bk_list{ $_ }->[B_STATUS_IDX] == S_GET } keys %bk_list;
@bk_lst_tmp = map { $bk_list{ $_ }->[B_BOOK_URL_IDX] } @bk_lst_tmp unless $query->query_page_id;
$query->wiki_pages( \@bk_lst_tmp );

# Get Books if there are some
if ( $query->page_count > 0 ) {
  $req_delay->delay_content unless $opts{f};  # no prev. req.
  gwtxt \%bk_list, $query, $req_delay;
}

# A sanity check
@bk_lst_tmp = grep { $bk_list{$_}->[B_STATUS_IDX] == S_GET or
                     $bk_list{$_}->[B_STATUS_IDX] == S_UNDEF } keys %bk_list;
if ( @bk_lst_tmp ) {
  print "\nNestahly se tyto požadované knihy:\n";
  print " '", $bk_list{ $_ }->[B_BOOK_TITLE_IDX], "', Wiki_Page_Id=$_, status=",
        $bk_list{$_}->[B_STATUS_IDX], "\n" for  @bk_lst_tmp;
}


sub rlst
{
  # file_hadler_to_read, hash_ref_to_store_data
  my ( $fh, $h ) = @_;
  while ( my $r = <$fh> ) {
    chomp $r;
    next unless $r;

    # CSV positions: 0 - book Id , 1 - page Id, 2 - book name URL, 3 - book name
    my ( $b_id, $p_id, $b_nm_url, $b_nm ) = parse_line( ',', 0, $r );
    die "'Wiki_Page_id'=$p_id na vstupu není unikátní" if exists $h->{ $p_id };
    $h->{ $p_id } = [ ( ( not defined $opts{f} ) ? S_UNDEF : S_GET ),
                      $b_id,
                      $b_nm_url,
                      $b_nm || $b_nm_url ];
  }
  die "Chyba čtení vstupního seznamu: $!" unless $fh->eof;
}


sub chkp
{
  # list_of_books_to_get, book_in_json
  my ( $b_lst, $b ) = @_;

  if ( $b->{missing} or $b->{invalid} ) {
    warn "Kniha nebyla nalezena na Wiki: Wiki_Page_Id='", $b->{pageid} // '',
         "' Title='", $b->{title} // '', "'";
    return;
  }

  my $page_id = $b->{pageid};
  unless( defined $page_id and defined $b->{title} ) {
    warn "Odpověď z Wiki neobsahuje atribut 'page_id' a/nebo 'title'";
    return;
  }

  unless ( exists $b_lst->{ $page_id } ) {
    warn "V opovědi z Wiki je kniha, která nebyla v dotazu: '",
         $b->{title}, "' s Wiki_Page_Id=$page_id, ignoruje se.";
    return;
  }

  # timestamp - S_UNDEF, content - S_GET
  unless ( $b_lst->{ $page_id }->[B_STATUS_IDX] == S_UNDEF or
           $b_lst->{ $page_id }->[B_STATUS_IDX] == S_GET ) {
    warn "V opovědi z Wiki je kniha '", $b->{title},
         "' s Wiki_Page_Id=$page_id, nebyla v posledním dotazu, status=",
         $b_lst->{ $page_id }->[B_STATUS_IDX], "), ignoruje se.";
    return;
  }
  return $page_id;
}  # chkp


sub gwtm
{
  # list_of_books_to_get, wiki_query_obj
  my ( $b_lst, $q, $s ) = @_;

  while ( my $j = $q->query_wiki )
  {
    for my $b ( @{ $j->{query}->{pages} } )
    {
      my $page_id = chkp $b_lst,  $b;
      next unless $page_id;

      my $mtime_wiki = str2time( $b->{revisions}->[0]->{timestamp} );
      unless ( defined $mtime_wiki ) {
        warn "Neznámý formát data a času: '",
             $b->{revisions}->[0]->{timestamp},
             "' v odpovědi pro knihu '", $b->{title},
             "', Wiki_Page_Id=$page_id, kniha se ignoruje.\n";
        next;
      }

      my $attr = $b_lst->{ $page_id };
      $attr->[B_STATUS_IDX] = S_GET;

      my $b_fn = $attr->[B_BOOK_ID_IDX] . '.json';
      if ( -e $b_fn ) {
        my $b_old = $q->json->rd_json_fn( $b_fn );
        my $mtime_local = str2time( $b_old->{revisions}->[0]->{timestamp} );
        unless ( defined $mtime_local ) {
          warn "Neznámý formát data a času v existujícím $b_fn: '",
               $b->{revisions}->[0]->{timestamp}, "\n";
          $mtime_local = 0;
        }

        if ( $mtime_local >= $mtime_wiki ) {
          print 'Kniha ', $attr->[B_BOOK_TITLE_IDX],
                " (Wiki_Page_id=$page_id): lokální soubor není starší než na Wiki, změna v '",
                $b->{revisions}->[0]->{timestamp}, "'\n";
          print "... ale je novější!\n" if $mtime_local != $mtime_wiki;
          $attr->[B_STATUS_IDX] = S_EXISTS;
        }
      }
    }
    $s->delay_tm if $q->page_count > 0;
  }
}  #gwtm


sub gwtxt
{
  # list_of_books_to_get, wiki_query_obj
  my ( $b_lst, $q, $s ) = @_;

  while ( my $j = $q->query_wiki )
  {
    for my $b ( @{ $j->{query}->{pages} } )
    {
      my $page_id = chkp $b_lst, $b;
      next unless $page_id;

      my $attr = $b_lst->{ $page_id };
      ( my $t = $b->{title} ) =~ s|Bible\s+kralická\s*/\s*||;
      unless ( $t eq $attr->[B_BOOK_TITLE_IDX] ) {
        warn "Není shoda v názvu knihy, očekáváno '",
           $attr->[B_BOOK_TITLE_IDX], "', nalezeno '",  $t ,
           "' pro Wiki_Page_Id=$page_id\n";
      }

      my $b_fn = $attr->[B_BOOK_ID_IDX] . '.json';

      my $h;
      open( $h, '>:raw', "$b_fn.tmp" ) and
        $q->json->wr_json_fh( $h, $b ) and
        close( $h ) or die "Chyba vytvoření JSON souboru: $!";

      if ( not $opts{B} and -e $b_fn ) {
        move $b_fn, "$b_fn.bak"
          or die "Chyba přejmenování 'bak' souboru: $!";
      }

      move "$b_fn.tmp", $b_fn
        or die "Chyba při přejmenování souboru: $!";

      print "'$t' -> $b_fn\n";

      warn "Kniha '$t' s Wiki_Page_Id=$page_id stažena vícekrát"
        if $attr->[B_STATUS_IDX] == S_DONE;
      $attr->[B_STATUS_IDX] = S_DONE;
    }

    $s->delay_content if $q->page_count > 0;
  }
}  # gwtxt


__END__


=pod

=encoding utf-8

=head1 NÁZEV

bk_get_wiki_json.pl - stažení poslední revize z Wikisource.org v JSON formátu.

=head1 VERZE

Verze 0.1

=head1 SYNOPSE

  cat Wiki_knihy_{Starý,Nový}_zákon.txt | ./bk_get_wiki_json.pl [ -S -B -f -t -h ]

=head1 PŘEPÍNACE

=over 4

=item -S

Nevkládat prodlevu mezi odesílané dotazy na Wikisource.org.

=item -B

Nedělat kopie souborů před přepsáním.

=item -f

Stahovat knihy bez ohledu na datum a čas poslední změny.

=item -t

V dotazech na Wikisource.org použít jméno knihy místo
jejího interního Id.

=item -h

Zobrazení stručné nápovědy.

=back

=head1 POPIS

Skript pomocí curl utility stahne knihy v JSON formátu
z L<https://cs.wikisource.org/wiki/Bible_kralická/>.
Skript čte na standardním vstupu seznam stránek (tj. knih Bible) ke stažení,
každý řádek obsahuje údaje o jedné knize v tomto CSV formátu:

  ID-knihy,Wiki-ID-knihy,jméno-knihy-v-URL,jméno_knihy

např.:

  Pis,4431,"Píseň_Šalamounova","Píseň Šalamounova"

Atribut C<jméno_knihy> se uvádí pokud C<jméno_knihy_v_URL> je rozdílné.

Skript nejprve zjistí datum a čas poslední změny knihy na Wikisource.org
a porovná ji s datem a časem poslední změny v lokálním souboru.
Je-li lokální soubor starší, přepíše se novým obsahem z Wikisource.org.

Pokud je na příkazové řádce uvedeno C<-f>, požadované knihy se stahnou
vždy, bez ohledu na datum a čas poslední změny.

Stažené soubory se ukládají do F<./json> adresáře,
pojmenované jsou podle vzoru: F<ID-knihy.json>.
Pokud není použit přepínač C<-B>, soubor s předchozí verzí se přejmenuje,
přidá se mu přípona C<.bak>.

Skript vkládá prodlevu několika vteřin mezi odesílané požadavky
na Wikisource.org, pokud není uveden přepínač C<-S>.

Skript při dotazování Wikisource.org implicitně používá atribut C<Wiki-ID-knihy>
(též C<page_id>), s volbou C<-t> se v dotazu použije atribut C<jméno_knihy>
(C<title>).
Atributy C<page_id> a C<title> knihy se dají najít ve zdrojovém
textu její HTML stránky na Wikisource.org.

=head1 AUTOR

E<lt>empekr@ghostmail.comE<gt>

=head1 COPYRIGHT A LICENCE

Copyright (c) 2015 empekr@ghostmail.com

Tento skript je svobodný software; můžete ho redistribuovat a/nebo upravovat
za stejných podmínek jako Perl 5.

This is free software; you can redistribute it and/or modify it under
the same terms as Perl 5 itself.

=cut

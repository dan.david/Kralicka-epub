#!/usr/bin/env perl

use File::Temp 'tempfile';
use EBook::EPUB;
use Data::UUID;
use Getopt::Std;
use File::Copy;
use Encode;
use POSIX;
use HTTP::Date;

use FindBin;
use lib "$FindBin::Bin/../perl5";

use BK::Json;
use BK::Books;

use strict;
use warnings;
use utf8;
use open qw ( :encoding(utf8) :std );

sub epub_date;
sub gen_uuid;
sub upd_cover_img;

{
  my $play_order = 0;

  sub book_navpoint
  {
    my ( $toc, $page, $page_title ) = @_;
    $play_order++;

    my $navpoint = $toc->add_navpoint(
         id => 'navPoit-' . $play_order,
         play_order => $play_order,
         content => $page,
         label => $page_title );
    return $navpoint;
  }
}

my %opts;
getopts( 'hnd:', \%opts ) or die "$0: chybné parametry skriptu";
if ( $opts{h} ) {
  print << "EOT";
Spuštění: $0 bk_parametry.json [ -h ]
  -h tento stručný popis
  -n sestavit jen Nový zákon
  -d datum sestavení e-knihy, iso8601 formát
Více v: perldoc $0
EOT
exit;
}

# epub internal components, real filenames and
# content labels are in bk_epub.json file (a parameter)
my %epub_parts = (
  text_dir => 'text',
  css_dir => 'styles',
  img_dir => 'images',
  Old_Testament_page => "sz.html",  # Old Testament first page
  New_Testament_page => "nz.html",  # New Testament first page
  copyright_page => "copyright.html",
  cover_page => "obalka.html",
  cover_img => "images/cover",  # ext. will be added later
);

# read definition of external components
my $data = BK::Json->new->rd_json_fn( shift || "bk_epub.json" );
if ( ( not defined $data->{epub}->{wiki_datum_cz} or length $data->{epub}->{wiki_datum_cz} == 0 )
      and
     defined $data->{epub}->{wiki_datum_iso} and length $data->{epub}->{wiki_datum_iso} > 0 ) {
  $data->{epub}->{wiki_datum_cz} =
    strftime( "%d. %m. %Y v %k:%M:%S", localtime( str2time( $data->{epub}->{wiki_datum_iso} ) ) );
  $data->{epub}->{wiki_datum_cz} =~ s/( ) +/$1/;
}
else {
  $data->{epub}->{wiki_datum_cz} = '';
}
$data->{epub}->{wiki_verze} //= $data->{epub}->{wiki_datum_iso};

# titles...
my $book_names = BK::Json->new->rd_json_fn(
      $FindBin::Bin . encode( 'UTF-8', "/../wiki/text/bk_jména_knih.json" ) );
my $bk = BK::Books->new;

my $epub = EBook::EPUB->new;
$epub->add_title( $data->{název} . ( $opts{n} ? " \x{2014} " . $data->{epub}->{nz_úvodní_stránka}->[1] : '' ) );
$epub->add_author( $data->{autor} );
$epub->add_translator( $data->{autor}, $data->{autor} );
$epub->add_language( 'cs' );
$epub->add_description( $data->{anotace} );
$epub->add_subject( $_ ) for @{ $data->{žánr} };
my $epub_date = epub_date $opts{d};
{
  local $SIG{__WARN__} = sub { };
  $epub->add_identifier( "urn:uuid:" . gen_uuid( $data ), 'UUID' );
}
# $epub->add_identifier( $data->{isbn}, 'ISBN' );
$epub->add_source( $data->{zdroj} );
$epub->add_publisher( $data->{epub}->{autor} );
$epub->add_date( '1613', 'original-publication' );
$epub->add_date( '2014-12-31', 'ops-publication' );
$epub->add_date( $epub_date, 'modification' );
$epub->add_rights( $data->{licence} );
$epub->add_format( 'epub+zip' );
$epub->add_meta_item( 'EBook::EPUB version', $EBook::EPUB::VERSION );
$epub->add_meta_item( 'Wikisource text version', $data->{epub}->{wiki_verze} );
$epub->add_meta_item( 'BKscript version', $data->{epub}->{verze} );
$epub->add_meta_item( 'External version', $data->{epub}->{verze_externí} );

$epub->copy_xhtml( $data->{epub}->{obálka_stránka},
                   "$epub_parts{text_dir}/$epub_parts{cover_page}",
                   linear => 'yes' );
upd_cover_img( \%epub_parts, $data->{epub}->{obálka} );
$epub->add_meta_item( 'cover',
                      $epub->copy_image( $data->{epub}->{obálka},
                                         $epub_parts{cover_img} ) );
# guide
$epub->guide->add_reference(
   { type => 'cover',
     href => "$epub_parts{text_dir}/$epub_parts{cover_page}",
     title => 'Obálka' } );
$epub->guide->add_reference(
   { type => 'copyright-page',
     href => "$epub_parts{text_dir}/$epub_parts{copyright_page}",
     title => $data->{epub}->{copyright_stránka}->[1] } );
$epub->guide->add_reference(
   { type => 'text',
     href => "$epub_parts{text_dir}/$epub_parts{Old_Testament_page}",
     title => $data->{epub}->{sz_úvodní_stránka}->[1] } ) unless $opts{n};
$epub->guide->add_reference(
   { type => 'text',
     href => "$epub_parts{text_dir}/$epub_parts{New_Testament_page}",
     title => $data->{epub}->{nz_úvodní_stránka}->[1] } );

# toc
{
  my $np;
  unless ( $opts{n} ) {
    $np = book_navpoint (
           $epub,
           "$epub_parts{text_dir}/$epub_parts{Old_Testament_page}",
           $data->{epub}->{sz_úvodní_stránka}->[1] );
    book_navpoint( $np,
                   "$epub_parts{text_dir}/$_.html",
                   $book_names->{$_}->[1] ) for $bk->ot_all_books;
  }
 
  $np = book_navpoint ( $epub,
                        "$epub_parts{text_dir}/$epub_parts{New_Testament_page}",
                        $data->{epub}->{nz_úvodní_stránka}->[1] );
  book_navpoint( $np,
                 "$epub_parts{text_dir}/$_.html",
                 $book_names->{$_}->[1] ) for $bk->nt_all_books;
  book_navpoint ( $epub,
                  "$epub_parts{text_dir}/$epub_parts{copyright_page}",
                  $data->{epub}->{copyright_stránka}->[1] );
}

# css
$epub->copy_stylesheet( $_->[0] , $_->[1] ) for ( @{ $data->{epub}->{css} } );

# The Old Testament
unless ( $opts{n} ) {
  $epub->copy_xhtml( $data->{epub}->{sz_úvodní_stránka}->[0],
                     "$epub_parts{text_dir}/$epub_parts{Old_Testament_page}" );
  for my $b_id ( $bk->ot_all_books ) {
    $epub->copy_xhtml( $data->{epub}->{kniha_adr} . "/$b_id.html",
                       "$epub_parts{text_dir}/$b_id.html" );
  }
}

# The New Testament
$epub->copy_xhtml( $data->{epub}->{nz_úvodní_stránka}->[0],
                   "$epub_parts{text_dir}/$epub_parts{New_Testament_page}" );
for my $b_id ( $bk->nt_all_books ) {
  $epub->copy_xhtml( $data->{epub}->{kniha_adr} . "/$b_id.html",
                     "$epub_parts{text_dir}/$b_id.html" );
}

# copyright
$epub->add_xhtml( "$epub_parts{text_dir}/$epub_parts{copyright_page}",
                  tmpl_repl( $data->{epub}->{copyright_stránka}->[0] ) );

# other img
for my $pic_f ( @{ $data->{epub}->{ostatní_obr} } ) 
{
  my $pic_fn = $epub_parts{img_dir} . '/' .
               substr( $pic_f->[1], rindex( $pic_f->[1], '/' ) + 1 );
  $epub->add_meta_item( $pic_f->[0],
                        $epub->copy_image( $pic_f->[1], $pic_fn ) );
}

if ( $epub->pack_zip( $data->{epub}->{název} . '.epub.tmp' ) ) {
  move $data->{epub}->{název} . '.epub.tmp',
       $data->{epub}->{název} . ( $opts{n} ? "_NZ" : '' ) . '.epub'
   or die "Chyba při přejmenování: $!";
}
else {
  warn "Chyba při sestavení ePUB knihy";
}


sub epub_date
{
  my ( $dt_str ) = @_;
  my $tm = time;
  if ( defined $dt_str ) {
    $tm = str2time $dt_str;
    die "Chybný datum nebo jeho formát: $dt_str" unless defined $tm;
  }
  my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime $tm;
  return sprintf "%d-%02d-%02d", $year + 1900, $mon + 1, $mday;
}


sub gen_uuid
{
  my ( $d ) = @_;

  my $du = Data::UUID->new();
  my $uuid = $du->create_from_name( NameSpace_URL, $d->{zdroj} );
  return $du->create_from_name_str( $uuid, $d->{epub}->{verze} . '|' . $d->{epub}->{wiki_verze} );
}


sub upd_cover_img
{
  my ( $d, $img ) = @_;
  my $i = rindex( $img, '.' );
  return if $i == -1;
  $d->{cover_img} .= substr( $img, rindex( $img, '.' ) );
}


sub tmpl_repl
{
  my ( $fn ) = @_;

  my $txt;
  {
    open( my $fh, '<', $fn ) or die "$!";
    local $/;
    $txt = <$fh>;
  }

  while ( $txt =~ m|< *tmpl_var +name *= *"([^"]+)" */>|g )
  {
    my $v = $data->{epub}->{$1};
    if ( defined $v ) {
      substr( $txt, $-[0], $+[0] - $-[0] ) = $v;
    }
  }
  $txt;
}


__END__

=pod

=encoding utf-8

=head1 NÁZEV

bk_epub.pl - vytvoření ePUB knihy.

=head1 VERZE

Verze 0.1

=head1 SYNOPSE

  ./bk_epub.pl bk_epub.json

=head1 POPIS

Skript sestaví e-knihu z kapitol Nového i Starého zákona
(pro každou knihu očekává jeden HTML soubor v adresáři F<$PWD/html>)
a z HTML souborů obsahující úvodní stránku celé Bible,
úvodní stránku Nového zákona, stránku o autorských právech
a úvodní stránku celé e-knihy.

Parametrem skriptu je soubor (v JSON formátu) s metadaty
a parametry knihy.

Parametry jsou v části C<epub>:

=over 4

=item C<název>

Jméno e-knihy. 

=item C<autor>

Autor e-knihy, nikoliv samotného textu.

=item C<verze>

Verze skriptů na generování e-knihy.

=item C<verze_externí>

Verze celé EPUB knihy.

=item C<wiki_datum_iso>

Obsahuje datum poslední modifikace textu Bible na Wikisource
použitého v e-knize v ISO8601 formátu.

=item C<wiki_verze>

Obsahuje verzi textu Bible na Wikisource.
Není-li vyplněný, použije se datum a čas z C<wiki_datum_iso>.

=item C<kniha_adr>

Adresář generovaných HTML souborů e-knihy.

=item C<css>

Pole dvojic jmen CSS souborů, které jsou v e-knize použity.
První z dvojice je aktuální jméno CSS souboru i s cestou,
druhé jméno ve dvojici je jméno a umístění souboru pro ePUB knihu -
takto se na CSS soubory odkazují HTML stránky e-knihy.

=item C<obálka_stránka>

Aktuální jméno souboru s obálkou.

=item C<sz_úvodní_stránka>

Dvojice obsahující aktuální jméno souboru s úvodní stránkou e-knihy
a její popisný text pro obsah e-knihy.

=item C<nz_úvodní_stránka>

Dvojice obsahující aktuální jméno souboru s úvodní stránkou Nového zákona
a její popisný text pro obsah e-knihy.

=item C<copyright_stránka>

Dvojice obsahující aktuální jméno souboru se stránku o autorských právech
a její popisný text pro obsah e-knihy.

=item C<obálka>

Aktuální jméno souboru s obrázkem obálky.
V knize se uloží jako F<images/Cover.xxx>,
kde C<xxx> je přípona ze jména souboru v tomto parametru.

=item C<ostatní_obr>

Seznam ostatních obrázků, které jsou v knize použity.
Pro každý obrázek je v seznamu dvojice: E<lt> meta-název, jméno souboru E<gt>.
Všechny obrázky se v knize ukládají pod stejným jménem souboru,
v podadresáři F<images>.

=back

Relativní jména uvedených souborů se vztahují k adresáři F<epub/>.

=head1 AUTOR

E<lt>empekr@ghostmail.comE<gt>

=head1 COPYRIGHT A LICENCE

Copyright (c) 2016 empekr@ghostmail.com

Tento skript je svobodný software; můžete ho redistribuovat a/nebo upravovat
za stejných podmínek jako Perl 5.

This is free software; you can redistribute it and/or modify it under
the same terms as Perl 5 itself.

=cut


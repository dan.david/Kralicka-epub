<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="no" encoding="utf-8" omit-xml-declaration="no"
     doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
     doctype-public="-//W3C//DTD XHTML 1.1//EN" />

  <xsl:variable name="EMSPACE" select="' '"/>  <!-- U+2003 -->
  <xsl:variable name="ENSPACE" select="' '"/>  <!-- U+2002 -->
  <xsl:variable name="NBSP" select="' '"/>    <!-- U+00a0 -->
  <xsl:variable name="NL" select="'&#xA;'"/>  <!-- N/L -->

  <xsl:variable name="kapitola_jméno">
    <xsl:choose>
      <xsl:when test="/kniha[@id='Z']"><xsl:text>Žalm</xsl:text></xsl:when>
      <xsl:otherwise><xsl:text>Kapitola</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- u zalmu cislujeme od 1, jinde od 2 -->
  <xsl:variable name="ID_od">
    <xsl:choose>
      <xsl:when test="/kniha[@id='Z']"><xsl:text>1</xsl:text></xsl:when>
      <xsl:otherwise><xsl:text>2</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates select="/kniha"/>
  </xsl:template>

  <xsl:template match="kniha">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
      <head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <title><xsl:value-of select="názvy/krátký"/></title>
        <xsl:element name="link">
          <xsl:attribute name="href"><xsl:call-template name="css-soubor"/></xsl:attribute>
          <xsl:attribute name="rel">stylesheet</xsl:attribute>
          <xsl:attribute name="type">text/css</xsl:attribute>
        </xsl:element>
      </head>
      <body>
        <div class="BK"><p><xsl:value-of select="$NBSP"/><br /><xsl:value-of select="$NBSP"/></p>

        <xsl:apply-templates select="názvy/rozložený"/>
 
        <xsl:if test="@kapitol>1">
          <p><xsl:value-of select="$NBSP"/></p>
          <div class="kapitoly">
            <xsl:call-template name="přehled-kapitol"/>
            <br />
          </div>
        </xsl:if>

        <xsl:for-each select="kapitola">
          <p><xsl:value-of select="$NBSP"/></p>

          <xsl:if test="noticka">
            <h5><xsl:apply-templates select="noticka/node()"/></h5>
            <p><xsl:value-of select="$NBSP"/></p>
          </xsl:if>

          <xsl:if test="/kniha/@kapitol>1">
            <xsl:element name="h3">
               <xsl:attribute name="id"><xsl:text>k</xsl:text><xsl:value-of select="/kniha/@id"/>_<xsl:value-of select="@id"/></xsl:attribute>
               <xsl:value-of select="$kapitola_jméno"/><xsl:value-of select="$NBSP"/><xsl:value-of select="@id"/><xsl:text>.</xsl:text>
            </xsl:element>
          </xsl:if>

          <xsl:if test="synopse">
            <h5><xsl:apply-templates select="synopse/node()"/></h5>
          </xsl:if>

          <p class="kap">
            <xsl:apply-templates select="verš"/>
          </p>

        </xsl:for-each>

        <xsl:apply-templates select="dovětek"/>

        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="verš[@ref='true']">
     <xsl:element name="span" namespace="http://www.w3.org/1999/xhtml">
        <xsl:attribute name="id"><xsl:text>v</xsl:text><xsl:value-of select="@celé_id"/></xsl:attribute>
        <xsl:if test="@id >= $ID_od">
          <xsl:value-of select="@id"/>
        </xsl:if>
     </xsl:element>
      <xsl:value-of select="$ENSPACE"/>
     <xsl:apply-templates select="./node()"/>
     <br xmlns="http://www.w3.org/1999/xhtml"/>
     <xsl:value-of select="$NL"/>
  </xsl:template>

  <xsl:template match="verš">

    <xsl:if test="@id >= $ID_od">
      <span xmlns="http://www.w3.org/1999/xhtml">
        <xsl:value-of select="@id"/>
      </span>
      <xsl:value-of select="$ENSPACE"/>
    </xsl:if>

    <xsl:apply-templates select="./node()"/>
    <br xmlns="http://www.w3.org/1999/xhtml"/>
    <xsl:value-of select="$NL"/>
  </xsl:template>

  <xsl:template match="dovětek">
    <p class="sep" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="./node()"/>
    </p>
  </xsl:template>

  <xsl:template match="node()">
    <xsl:copy>
      <xsl:apply-templates select="./node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="v_ref">
     <xsl:element name="a" xmlns="http://www.w3.org/1999/xhtml">
        <xsl:attribute name="href">#v<xsl:value-of select="@v_id"/></xsl:attribute>
        <xsl:value-of select="text()"/>
     </xsl:element>
  </xsl:template>

  <xsl:template match="k_ref">
     <xsl:element name="a" xmlns="http://www.w3.org/1999/xhtml">
        <xsl:attribute name="href">
          <xsl:if test="string-length(@b_id) != 0"><xsl:value-of select="@b_id"/>.html</xsl:if>#k<xsl:value-of select="@k_id"/></xsl:attribute>
        <xsl:value-of select="text()"/>
     </xsl:element>
  </xsl:template>

  <xsl:template match="alt_text">
     <i xmlns="http://www.w3.org/1999/xhtml">
       <xsl:apply-templates select="./node()"/>
     </i>
  </xsl:template>

  <xsl:template match="alt_text_2">
     <!-- ignoruje se -->
  </xsl:template>

  <xsl:template match="iniciála">
     <strong class="BKi" xmlns="http://www.w3.org/1999/xhtml">
       <xsl:apply-templates select="./node()"/>
     </strong>
  </xsl:template>

  <xsl:template match="iniciála_vnitřní">
     <br xmlns="http://www.w3.org/1999/xhtml"/><strong class="BKi" xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="."/></strong>
  </xsl:template>

  <xsl:template name="css-soubor">
    <xsl:choose>
      <xsl:when test="@id != 'Z'">
        <xsl:text>../styles/bk.css</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>../styles/bk.css</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="přehled-kapitol">

     <xsl:for-each select="kapitola">

        <xsl:choose>
           <xsl:when test="$ID_od>@id"></xsl:when>

           <xsl:when test="@id=/kniha/@kapitol">

              <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
                 <xsl:attribute name="href">#k<xsl:value-of select="/kniha/@id"/>_<xsl:value-of select="@id"/></xsl:attribute>
                 <xsl:value-of select="@id"/>
              </xsl:element>

            </xsl:when>

            <xsl:otherwise>
               <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
                  <xsl:attribute name="href">#k<xsl:value-of select="/kniha/@id"/>_<xsl:value-of select="@id"/></xsl:attribute>
                  <xsl:value-of select="@id"/>
               </xsl:element>

               <xsl:choose>
                 <xsl:when test="@id &lt; 9"><xsl:value-of select="$EMSPACE"/></xsl:when>
                 <xsl:when test="@id &lt; 99"><xsl:value-of select="$ENSPACE"/></xsl:when>
               </xsl:choose>

               <xsl:choose>
                 <xsl:when test="@id mod 5 = 0">
                   <xsl:value-of select="$NL"/>
                 </xsl:when>
                 <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
               </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>

     </xsl:for-each>

  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_kniha_1']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_kniha_3']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h5 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h5>
         <h4 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='3']"/></h4>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_kniha_4']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h2>
         <h5 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='3']"/></h5>
         <h4 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='4']"/></h4>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_evangelium_3']">
         <h4 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h4>
         <h5 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h5>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='3']"/></h2>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_skutky']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h2>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_epištola_2']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h2>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_epištola_3']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='3']"/></h2>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_proroci_2']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h2>
  </xsl:template>

  <xsl:template match="názvy/rozložený[@typ='bk_zjevení']">
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='1']"/></h2>
         <h2 xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="řádek[@n='2']"/></h2>
  </xsl:template>

</xsl:stylesheet>

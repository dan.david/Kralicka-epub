
## Kralička.epub

> The Bible of Kralice, also called Kralice Bible
> (Czech: Bible kralická) was the first complete
> translation of the Bible from the original languages
> into the Czech language.
Source: [Wikipedia](https://en.wikipedia.org/wiki/Bible_of_Kralice)

This Git repository contains scripts to create
Kralice Bible as an e-book in the EPUB v2 format.
The e-book consists of the text available on
[Wikisource.org](https://cs.wikisource.org/wiki/Bible_kralick%C3%A1) and
excerpts that are in printed books.

The EPUB book is created in several steps:

1. Text of the Bible is downloaded from Wikisource.org in the JSON format,
one book of the Bible per a file. A command to launch:
 
    ~/Kralicka-epub/wiki $ make get_all

The script downloads only modified books of the Bible -
for each book of the Bible the script compares its modification date and time on Wikisource.org with local one.

2. Optionally - downloaded text is compared with previous one
to see changes:

    ~/Kralicka-epub/wiki $ make diff

3. Downloaded text is transformed to XTHML and the e-book is created:

    ~/Kralicka-epub $ make

The e-book was verified in [Aldiko](http://www.aldiko.com)
and [Calibre](http://calibre-ebook.com).

Tools used to build the e-book: Bash, GNU make, Perl v5.22, xsltproc and curl.

Note: There is Makefile_html that performs similar steps but
HTML files are dowloaded as the source of the Bible instead of the JSON files.

---

Tento Git archiv obsahuje několik skriptů, které vytvoří EPUB knihu
(ver. 2) Bible Kralické z volně dostupného textu na
[Wikisource.org](https://cs.wikisource.org/wiki/Bible_kralick%C3%A1).
Výsledná e-kniha obsahuje i záhlaví kapitol, jak bývají v tištěné knize,
viz též *O této e-knize* v e-knize.

Vzhled a funkčnost e-knihy byly ověřeny na čtečce
[Aldiko](http://www.aldiko.com) (Android verse)
a [Calibre](http://calibre-ebook.com) na Linuxu.

K sestavení e-knihy byl použit Bash, GNU make, Perl, xsltproc a curl.

Celý postup je rozdělen na několik kroků:

1. Stažení knih Starého i Nového zákona z Wikisource.org do
adresáře `Kralicka-epub/wiki/json/`,
každá kniha je v samostatném souboru
(skript `Kralicka-epub/wiki/bk_get_wiki_json.pl`).

Pokud soubor před stažením již existuje, k jeho jménu se přidá přípona `.bak`.

2. Volitelně - ověření změn ve stažených souborech:
porovná se poslední stažená revize s předchozí, která je v souboru
s příponou `.bak`
(skript `Kralicka-epub/wiki/misc/bk_cmp.pl`).

3. Převedení každé stažené knihy z Wikisource.org do XML dokumentu,
výsledek se uloží to adresáře `Kralicka-epub/wiki/xml/`
(skript `Kralicka-epub/wiki/bk_ext_wiki.pl`).

4. XML transformací (soubor `Kralicka-epub/epub/bk_epub.xslt`) se z každého
XML dokumentu vygeneruje XHTML
soubor pro e-knihu, výsledek se uloží do adresáře
`Kralicka-epub/epub/html/`.

5. Sestavení e-knihy `Kralicka-epub/epub/Kralička.epub`
(skript `Kralicka-epub/epub/bk_epub.pl`).

Krom vstupního JSON formátu je možné použít i HTML:

* na stahování se použije skript
`Kralicka-epub/wiki/bk_get_wiki_html.pl`,
soubory se ukládají do adresáře `Kralicka-epub/wiki/html/`

* skript `Kralicka-epub/wiki/bk_ext_wiki.pl` je třeba spustit s přepínačem `-H`

Viz též soubor `Kralicka-epub/wiki/Makefile_html`.

### Stažení z Wikisource.org

Následující příkazy, spuštěné v adresáři `Kralicka-epub/wiki/`,
stahnou všechny knihy bible v JSON formátu, každá kniha
v jednom souboru:

    make get_all

nebo:

    make get_Old   # Starý zákon
    make get_New   # Nový zákon

nebo:

    cat Wiki_knihy_{Starý,Nový}_zákon.txt | ./bk_get_wiki_json.pl


případně selektivně, např.:

    grep ^J, Wiki_knihy_Nový_zákon.txt | ./bk_get_wiki_json.pl

### Transformace a sestavení e-knihy

V adresáři `Kralicka-epub/` spustit:

    make

### Úpravy textu

Na dvou místech bylo přesunuto několik veršů do předcházející resp.
následující kapitoly pro sjednocení s tištěnou knihou.
Jde o tyto dva případy:

1. V knize Jonáš byl původní verš Jon1:17 přesunut na Jon2:1
a ostatní verše v Jon2 přečíslovány +1.

2. V knize Job byly původní verše Jb40:1-5 přesunuty na Jb39:31-35
a zbylé verše v Jb40 přečíslovány -5.

Po stažení knihy Job resp. Jonáš se uvedené úpravy provedou
automaticky.

### Porovnání nových revizí

Příkaz:

   make diff

spuštěný v adresáři `Kralicka-epub/wiki` porovná poslední stažené
JSON soubory s předchozími.

Ručním spuštěním porovnávajícího skriptu je možné porovnávat
HTML a JSON soubory, i vzájemně.

### JSON vs. HTML

Stahování HTML stránek by mělo umožnit snadné inkrementální změny
(stahování jen změněných HTML stránek), ale je to zřejmě jediná výhoda
oproti JSON formátu.

JSON formát je snadnější na zpracování a tím i spolehlivější,
přenáší se méně dat, efektivnějším způsobem,
proto je preferovaný.

### Prostředí, Perl a závislosti

Skripty byly napsány a testovány na Linuxu,
předpokládají UTF-8 terminál (locale: cs_CZ.UTF-8, C.UTF-8 apod.).

Verze Perlu byla 5.22 a použité moduly z metacpan.org
(krom "core" modulů) jsou:

* Moose
* EBook::EPUB
* XML::Writer
* Archive::Zip
* Data::UUID
* HTML::Parser
* HTML::Tagset
* HTTP::Date

Ukázka sestavené e-knihy je na [MobileRead.com](http://www.mobileread.com/forums/showthread.php?t=269392).

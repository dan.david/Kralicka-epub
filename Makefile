
.PHONY: all clean

all clean:
	make -C ./wiki $@
	make -C ./epub $@



package BK::Lib;

use utf8;

sub vlnka
{
  $_[0] =~ s|\b[óksvzoui][\]}]?\K |\x{A0}|gi;
  $_[0] =~ s|\b[A][\]}]?\K |\x{A0}|g;
  return $_[0];
}

sub vlna {
  $_[0] =~ s/\r?\n/ /g;
  $_[0] =~ s/^ +//;
  $_[0] =~ s/ +/ /;
  $_[0] =~ s/ +$//;
  $_[0] =~ s/([,.;])([[:alpha:]\[])/$1 $2/g;
  $_[0] =~ s|(?<=[“\w]) (?=–)|\x{A0}|g;
  $_[0] =~ s|(?<=[0-9]) (?=[0-9]{3})|\x{A0}|g;
  # return vlnka( $_[0] );
  return vlnka $_[0];
}

1;

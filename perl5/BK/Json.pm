
package BK::Json;

use Moose;
use JSON::PP;
use Carp;
use utf8;

has json => (
  is => 'rw',
  isa => 'Object',
);

sub BUILD
{
  my ( $self ) = @_;
  $self->json( JSON::PP->new->utf8->allow_barekey->relaxed );
}

sub rd_json_fh {
  my ( $self, $fh ) = @_;

  local $/;
  my $json_data = <$fh>;
  croak "chyba čtení ze souboru: $!" unless $fh->eof;
  return $self->json->decode( $json_data );
}

sub rd_json_fn {
  my ( $self, $fn ) = @_;

  open( my $fh, '<', $fn ) or croak "soubor nelze otevřít: '$fn': $!";
  return $self->rd_json_fh( $fh );
}

sub wr_json_fh {
  print {$_[1]} $_[0]->json->encode( $_[2] ) or croak "chyba zápisu do souboru: $!";
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;


package BK::Initials;

use Moose;
use utf8;

has inits => (
  is => 'rw',
  isa => 'HashRef',
  default => sub { {} },
);

has verse_no => (
  is => 'rw',
  isa => 'HashRef',
  default => sub { { I1 => 1,
                     I2 => 2,
                     I3 => 1,
                     I4 => 1,
                     I5 => 1,
                     I6 => 1,
                     I7 => 1,
                   } },
);

sub BUILD
{
  my $self = shift;

  # iniciala na 1. znaku 1. verse (z1)
  $self->inits->{Z}->{$_} = 'I1' for
            qw {
                 1 2 10
                 33
                 43
                 71
                 91 93 94 95 96 97 99
                 104 105 106 107
                 114 115 116 117 118
                 136 137
                 147
               };

  # iniciala na 1. znaku 2. verse (z2)
  $self->inits->{Z}->{$_} = 'I2' for qw { 22 };

  # iniciala na 1. znaku 2. vety 1. verse ( v knize jde o 2 verse ) (z3)
  $self->inits->{Z}->{$_} = 'I3' for
            qw {
                 3 4 5 6 7 8 9
                 12 13 19 20
                 21 30
                 31 34 36 38 39 40
                 41 42 44 46 47 48 49
                 53 55 56 57 58 59
                 61 62 63 64 65 67 68 69 70
                 75 76 77 80
                 81 83 84 85 88 89
                 92
                 102 108
                 140
                 142
               };

  # iniciala na 1. znaku 2. vety 1. verse ( v knize jde take o jeden vers ) (z4)
  $self->inits->{Z}->{$_} = 'I4' for
            qw {
                 11 14 15 16 17
                 23 24 25 26 27 28 29
                 32 35 37
                 50
                 66
                 72 73 74 78 79
                 82 86 87 90
                 98 100
                 101 103 109 110
                 111 112 113 119 120
                 121 122 123 124 125 126 127 128 129 130
                 131 132 133 134 135 138 139
                 141 143 144 145 146 148 149 150
               };

  # iniciala na 1. znaku 2. vety 1. verse (z5)
  $self->inits->{Z}->{$_} = 'I5' for qw { 51 52 54 60 };

  # iniciala na 1. znaku 3. vety 1. verse (z6)
  $self->inits->{Z}->{$_} = 'I6' for qw { 45 };

  # text "rekl: Z"
  $self->inits->{Z}->{$_} = 'I7' for qw ( 18 );
}

sub get_verse_no
{
  my ( $self, $book_id, $chapter_id ) = @_;
  return 1 unless
    defined $self->inits->{$book_id} and
    defined $self->inits->{$book_id}->{$chapter_id};
  return $self->verse_no->{ $self->inits->{$book_id}->{$chapter_id} } // 1;
}

sub get_ini_type
{
  my ( $self, $book_id, $chapter_id ) = @_;
  return 'I1' unless
    defined $self->inits->{$book_id} and
    defined $self->inits->{$book_id}->{$chapter_id};
  return $self->inits->{$book_id}->{$chapter_id};
}

sub tag_ini
{
  my ( $self, $book_id, $chapter_id, $r_txt ) = @_;

  my $i_tp = $self->get_ini_type( $book_id, $chapter_id );

  if ( $i_tp eq 'I1' or $i_tp eq 'I2' ) {
    no warnings qw{ uninitialized };
    $$r_txt =~ s/^([\[{])?(ch|[[:alpha:]])/$1B<\U$2>/i;
    use warnings qw{ uninitialized };
    return;
  }

  if ( $i_tp =~ '^I[345]' ) {
    $$r_txt =~ s/[.?!](?:[\]}])?\K\s+(ch|[[:alpha:]])/U<\U$1>/i;
    return;
  }

  if ( $i_tp eq 'I6' ) {
    # $$r_txt =~ s/(?<=\.(?:[^.]+).)\s+(ch|[[:alpha:]])/U<\U$1>/i;
    $$r_txt =~ s/\.[^.]+\.\K\s+(ch|[[:alpha:]])/U<\U$1>/i;
    return;
  }

  if ( $i_tp eq 'I7' ) {
    $$r_txt =~ s/(?<=řekl:)\s+Z/U<Z>/;
    return;
  }
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;

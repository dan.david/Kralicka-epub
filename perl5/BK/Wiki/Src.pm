
package BK::Wiki::Src;

use Moose;

use BK::Wiki::List;

has wiki_encoding => (
  is => 'rw',
  isa => 'Str',
  default => 'UTF-8',
);

has bk => (
  is => 'rw',
  isa => 'BK::Wiki::List', # list of chapters
);

has bk_meta => (
  is => 'rw',
  isa => 'HashRef',
  default => sub { {} },
);

__PACKAGE__->meta->make_immutable;

no Moose;

1;

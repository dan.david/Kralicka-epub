
package BK::Wiki::RqGap;

use Moose;
use utf8;

has timestamp_sleep => (
  is => 'rw',
  isa => 'Int',
  default => 51,
);

has content_sleep => (
  is => 'rw',
  isa => 'Int',
  default => 59,
);

has rand_sleep => (
  is => 'rw',
  isa => 'Int',
  default => 13,
);

sub BUILDARGS
{
  my ( $self, $cli_opt ) = @_;
  my %args;
  $args{timestamp_sleep} = $args{content_sleep} = 0 if defined $cli_opt->{S};
  return \%args;
}

sub _delay
{
  my ( $self, $secs ) = @_;
  return unless $secs > 0;
  $secs = int( rand( $self->rand_sleep ) + $secs );
  print "Pauza na $secs vteřin...\n";
  sleep $secs;
}

sub delay_tm {
  my $self = shift;
  $self->_delay( $self->timestamp_sleep );
}

sub delay_content
{
  my $self = shift;
  $self->_delay( $self->content_sleep );
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;

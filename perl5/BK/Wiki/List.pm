
package BK::Wiki::List;

use Moose;
use Moose::Meta::Attribute::Native::Trait::Array;

has name => (
  is => 'ro',
  isa => 'Str',
  required => 1,
);

has items => (
  is => 'bare',
  isa =>'ArrayRef[Object]',
  traits => ['Array'],
  default => sub { [] },
  handles => {
    add_item => 'push',
    all_items => 'elements',
    get_item => 'get',
    item_count  => 'count',
    single_iter => 'natatime',
  },
);

sub last_item { return $_[0]->get_item( $_[0]->item_count - 1 ); }

__PACKAGE__->meta->make_immutable;

no Moose;

1;

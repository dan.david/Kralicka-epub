
package BK::Wiki::Json;

use Moose;
use Carp;

use BK::Json;
use BK::Wiki::Text;
use BK::Wiki::Verse;
use BK::Wiki::List;

extends 'BK::Wiki::Src';

use utf8;

# it is utf8, no action is required
sub text_utf8 { return $_[1]; }

sub parse
{
   my ( $self, $wiki_fh ) = @_;

   my $j = BK::Json->new;
   my $d = $j->rd_json_fh( $wiki_fh );

   croak "Json dokument obsahuje víc než jednu revizi" if @{ $d->{revisions} } > 1;

   my $title = $d->{title};
   $title =~ s|Bible\s+kralická\s*/\s*||;
   $self->bk( BK::Wiki::List->new( name => $title ) );

   my $last_chap_id ;
   for my $r ( split( "\n", $d->{revisions}->[0]->{content} ) )
   {
     next unless $r =~ /^\s*\{\{[Vv]erš\|kapitola=(\d+)\|verš=(\d+)\}\}\s*/p;
     my ( $ch_no, $v_no ) = ( $1, $2 );
     my $v_txt = ${^POSTMATCH};

     if ( not defined $last_chap_id or
          $last_chap_id != $ch_no ) {
       $self->{bk}->add_item( BK::Wiki::List->new( name => $ch_no ) );
       $last_chap_id = $ch_no;
     }
     $self->bk()->last_item()->add_item( BK::Wiki::Verse->new( v_id => $v_no, text => $v_txt ) );
   }

   $self->bk_meta->{timestamp} = $d->{revisions}->[0]->{timestamp} // '';
   $self->bk_meta->{author} = $d->{revisions}->[0]->{user} // '';

   return $self;

} # parse

__PACKAGE__->meta->make_immutable;

no Moose;

1;

__END__

{{verš|kapitola=1|verš=1}} Text

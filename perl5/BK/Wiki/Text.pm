
package BK::Wiki::Text;

# use namespace::autoclean;
use Moose;

has text => (
  is => 'ro',
  isa => 'Str',
  traits => ['String'],
  default => '',
  handles => {
    add_text => 'append',
    clear_text => 'clear',
  }
);

__PACKAGE__->meta->make_immutable;

no Moose;

1;

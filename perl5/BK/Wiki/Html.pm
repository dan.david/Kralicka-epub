
package BK::Wiki::Html;

use Moose;
use Moose::Util::TypeConstraints;
use HTML::Parser;
use Encode;

use BK::Wiki::Text;
use BK::Wiki::Verse;
use BK::Wiki::List;

extends 'BK::Wiki::Src';

has stus => (
  is => 'rw',
  isa => enum( [ qw( sNIL sH sK sV sT ) ] ),
  # predicate => 'is_state_set',
  clearer => 'clear_state',
  writer => 'set_state',
  default => 'sNIL',
  lazy => 1,
  trigger => sub { $_[0]->buf->clear_text; },
);

has buf => (  # a text buffer
  is => 'rw',
  isa => 'BK::Wiki::Text',
  default => sub { BK::Wiki::Text->new },
);

has parser => (
  is => 'rw',
  isa => 'Object',  # HTML::Parser
);

sub start
{
  my ( $self, $tag, $attr ) = @_;

  if ( $tag eq 'span' and ( $attr->{class} // '' ) eq 'mw-headline' ) {
    # beginning of a chapter
    $self->set_state( 'sK' );
    return;
  }
  elsif ( $tag eq 'sup' ) {
    # start of a verse (number)
    $self->set_state( 'sV' );
    return;
  }
  elsif ( $tag eq 'br' and $self->stus eq 'sT' ) {
    # <br /> terminates text of a verse
    ( my $t = $self->buf->text ) =~ s/^\s+//;
    $self->bk()->last_item()->last_item()->add_text( $t );
  }
  $self->clear_state if $self->stus ne 'sNIL';
} # start

sub start_book
{
  my ( $self, $tag, $attr ) = @_;
  if ( $tag =~ /h\d/ and defined $attr->{class} and $attr->{class} eq 'firstHeading' ) {
    # beginning of a book
    $self->set_state( 'sH' );
    $self->parser->handler( start => sub { start( $self, @_ ) }, "tagname, attr" );
  }
  elsif ( $tag eq 'meta' and exists $attr->{charset} ) {
    $self->wiki_encoding( $attr->{charset} );
  }
} # start_book

sub end
{
  my ( $self, $tag ) = @_;

  if ( $self->stus eq 'sH' and $tag =~ /^h/ ) {
    # book name (id) has been read
    ( my $t = $self->buf->text ) =~ s|^.*/||; 
    $self->bk( BK::Wiki::List->new( name => $t ) );
  }
  elsif ( $self->stus eq 'sK' and $tag eq 'span' ) {
    # start of a chapter has been read
    if ( $self->buf->text =~ /(\d+)\./ ) {
      my $ch_no = $1;  # chapter no.
      $self->{bk}->add_item( BK::Wiki::List->new( name => $ch_no ) );
    }
  }
  elsif ( $self->stus eq 'sT' and $tag eq 'p' ) {
    # </p> terminates last verse in a chapter
    ( my $t = $self->buf->text ) =~ s/^\s+//;  # rm leading whitechars
    $self->bk()->last_item()->last_item()->add_text( $t );
  }
  elsif ( $self->stus eq 'sV' and $tag eq 'sup' ) {
    # verse number has been read, text follows...
    $self->bk()->last_item()->add_item( BK::Wiki::Verse->new( v_id => $self->buf->text ) );
    $self->set_state( 'sT' );  # next - read text of the verse
    return;
  }
  $self->clear_state if $self->stus ne 'sNIL';
} # end

sub text
{
  my ( $self, $txt ) = @_;
  return if $self->stus eq 'sNIL';
  $self->buf->add_text( $txt );
} # text

sub parse
{
   my ( $self, $wiki_fh ) = @_;

   $self->parser( HTML::Parser->new(
      api_version => 3,
      start_h => [ sub { start_book( $self, @_ ) }, "tagname, attr"],
      end_h   => [ sub { end( $self, @_ ) },   "tagname"],
      text_h  => [ sub { text( $self, @_ ) }, "text" ] ,
      # marked_sections => 1,
    ) );
  $self->parser->empty_element_tags( 1 );
  $self->parser->parse_file( $wiki_fh );
  return $self;
} # parse

sub text_utf8
{
  my ( $self, $txt ) = @_;
  return decode( $self->wiki_encoding, $txt );
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;

__END__

Encoding: <meta charset="UTF-8" />
Book: <h1 id="firstHeading" class="firstHeading" lang="cs">Bible kralická/Jmeno</h1>
Chapter: <span class="mw-headline" id="Kapitola_ID.">Kapitola ID.</span>
Verse: <p><sup style="color: #090;" id="1:1">ID</sup> TEXT <br />

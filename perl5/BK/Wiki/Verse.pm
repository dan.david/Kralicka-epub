
package BK::Wiki::Verse;

use Moose;

extends 'BK::Wiki::Text';

has v_id => (
  is => 'ro',
  isa => 'Int',
  required => 1,
);

__PACKAGE__->meta->make_immutable;

no Moose;

1;

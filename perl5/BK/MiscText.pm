
package BK::MiscText;

use Moose;

extends 'BK::Json';

has [ qw / chapter_syn chapter_pre / ] => (
  is => 'rw',
  isa => 'HashRef[HashRef[Str]]',
);

has book_post => (
  is => 'rw',
  isa => 'HashRef[Str]',
  traits => ['Hash'],
  handles => {
    get_book_post => 'get',
  },
);

has book_title_struct_len => (
  is => 'rw',
  isa => 'HashRef[Int]',
  traits => ['Hash'],
  handles => {
    get_book_title_len => 'get',
  },
);

has book_name => (
  is => 'rw',
  isa => 'HashRef[ArrayRef]',
  traits    => ['Hash'],
  handles   => {
    get_book_name => 'get',
  },
);

sub init
{
  my ( $self, $ch_syn_fn, $ch_pre_fn, $b_post_fn, $b_name_fn,
       $b_title_fn ) = @_;
  $self->chapter_syn( $self->rd_json_fn( $ch_syn_fn ) ) if $ch_syn_fn;
  $self->chapter_pre( $self->rd_json_fn( $ch_pre_fn ) ) if $ch_pre_fn;
  $self->book_post( $self->rd_json_fn( $b_post_fn ) ) if $b_post_fn;
  $self->book_name( $self->rd_json_fn( $b_name_fn ) ) if $b_name_fn;
  $self->book_title_struct_len( $self->rd_json_fn( $b_title_fn ) ) if $b_title_fn;
  return $self;
}

# find chapter's synopsis for passed chapter Id: book_id chapter_id
sub get_chapter_syn
{
  my ( $self, $book_id, $ch_id ) = @_;

  return unless $self->chapter_syn;
  return unless $self->chapter_syn->{$book_id};
  return $self->chapter_syn->{$book_id}->{$ch_id};
}

sub get_chapter_pre
{
  my ( $self, $book_id, $ch_id ) = @_;

  return unless $self->chapter_pre;
  return unless $self->chapter_pre->{$book_id};
  return $self->chapter_pre->{$book_id}->{$ch_id};
}

sub set_chapter_syn
{
  my ( $self, $book_id, $ch_id, $text ) = @_;
  return $self->chapter_syn->{$book_id}->{$ch_id} = $text;
}

sub set_chapter_pre
{
  my ( $self, $book_id, $ch_id, $text ) = @_;
  return $self->chapter_pre->{$book_id}->{$ch_id} = $text;
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;

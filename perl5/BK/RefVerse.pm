
package BK::RefVerse;

use Moose;
use Text::Levenshtein::Damerau::PP qw( pp_edistance );
use utf8;

extends 'BK::Json';

has edist => (
  is => 'rw',
  isa => 'Int',
  default => 5,
);

has v_r => (
  is => 'rw',
  isa => 'HashRef[Str]',
  traits => ['Hash' ],
  handles => {
    get_r_verse => 'get',
  },
  default => sub { {} },
);

sub init
{
  my ( $self, $v_ref_fn ) = @_;
  $self->v_r( $self->rd_json_fn( $v_ref_fn ) ) if $v_ref_fn;
  return $self;
}

sub v_r_mod
{
  my ( $self, $v_txt ) = @_;
  $v_txt =~ s|<[^>]+>||g;
  $v_txt =~ s/^\s+//;
  $v_txt =~ s/\s+$//;
  $v_txt =~ s/\s+/ /;
  $v_txt =~ s/ / /g;  # nbsp -> sp
  return $v_txt;
}

# find chapter's synopsis for passed chapter Id: book_id chapter_id
sub v_r_match
{
  my ( $self, $v_id, $v_txt ) = @_;

  return undef unless exists $self->v_r->{$v_id};
  my $t = $self->v_r->{$v_id};
  $t =~ s/\s*\{.+?\}//g;

  return pp_edistance( $v_txt, $t, $self->edist );
}

__PACKAGE__->meta->make_immutable;

no Moose;

1;


package BK::Regexp;

use Moose;

# Internal POD markups
has pod_tag_cap => (
  is => 'ro',
  isa => 'RegexpRef',
  default =>  sub { qr /( [LIBYU]<
                   (?:
                     (?>[LIBYU]+[^<>]*)
                     |
                     (?>[^<>LIBYU]*)
                     |
                     (?1)
                   )*
                   >
                 )
                /x },
);

__PACKAGE__->meta->make_immutable;

no Moose;

1;
